import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


from coffee_app.api_admin.__init__ import app
if __name__ == "__main__":
        app.run()
