from werkzeug.contrib.cache import MemcachedCache
import redis
import memcache

redis_port = 26379
r = redis.StrictRedis(host='localhost', port=redis_port, db=2)
r_sms = redis.StrictRedis(host='localhost', port=redis_port, db=3)
r_time = redis.StrictRedis(host='localhost', port=redis_port, db=4)
#cache = MemcachedCache(['127.0.0.1:11211'])


MAX_LENGTH_FILE = 20971520  # 1024*1024*20
CODE_ERROR = 404
COUNT_SMS = 5

push_key_android = ''
push_key_ios = ''

# roles
role_admin_app = 'admin_app'
role_admin_network = 'admin_network'
role_worker_network = 'worker_network'
role_client = 'client'

type_work = 2
# 1 - local
# 2 - test server
# 3 - production
if type_work == 1:
    user = 'goods_deliver'
    database = 'coffee_network'
    password = '86429731'
    host = 'localhost'
    port = '5430'

    str_path_to_file = '/files/'
    base_url = 'http://127.0.0.1:6968'
    str_connect_to_db = "dbname='{}' user='{}' host='{}' password='{}' port='{}'".format(database,
                                                                                         user,
                                                                                         host,
                                                                                         password,
                                                                                         port)
    domain_api = '/coffee/app/api/v1'
    domain_api_mobile = '/coffee/app/mobile/api/v1'
    domain_api_mobile_worker = '/coffee/app/mobile/api/v1/barista'
    domain_web = ''
    MEDIA_FOLDER = '/home/denis/PycharmProjects/coffee_app'
    domain_photo = ''
    mail_app = ''
    password_mail_app = ''
    path_to_celery = ''
    secret_word = 'secret'
    algorithm_encode = 'HS256'
    cache = memcache.Client(['127.0.0.1:11211'])
    path_to_answer_mobile_api = '/coffee_app/api_mobile/answer_server'
elif type_work == 2:
    user = 'goods_deliver'
    database = 'coffee_network'
    password = '86429731'
    host = 'localhost'
    port = '5432'

    str_path_to_file = '/files/'
    base_url = 'http://178.159.110.21:90'
    str_connect_to_db = "dbname='{}' user='{}' host='{}' password='{}' port='{}'".format(database,
                                                                                         user,
                                                                                         host,
                                                                                         password,
                                                                                         port)
    domain_api = '/coffee/app/api/v1'
    domain_api_mobile = '/coffee/app/mobile/api/v1'
    domain_api_mobile_worker = '/coffee/app/mobile/api/v1/barista'
    domain_web = ''
    MEDIA_FOLDER = '/home/den/app_coffee'
    domain_photo = '/coffee_app'
    mail_app = ''
    password_mail_app = ''
    path_to_celery = ''
    secret_word = 'TUyr4fnXce0ZF2vVbbOA'
    algorithm_encode = 'HS256'
    cache = MemcachedCache(['127.0.0.1:11211'])
elif type_work == 3:
    user = 'goods_deliver'
    database = 'coffee_network'
    password = '86429731'
    host = 'localhost'
    port = '5430'

    str_path_to_file = '/files/'
    base_url = ''
    str_connect_to_db = "dbname='{}' user='{}' host='{}' password='{}' port='{}'".format(database,
                                                                                         user,
                                                                                         host,
                                                                                         password,
                                                                                         port)
    domain_api = ''
    domain_api_mobile = '/coffee/app/mobile/api/v1'
    domain_api_mobile_worker = '/coffee/app/mobile/api/v1'
    domain_web = ''
    MEDIA_FOLDER = ''
    domain_photo = ''
    mail_app = ''
    password_mail_app = ''
    path_to_celery = ''
    secret_word = 'secret'
    algorithm_encode = 'HS256'
