import asyncpg
import random
from datetime import datetime, timedelta
from coffee_app.api_mobile_barista.answers import error_in_connect,\
    error_in_select,\
    good_entry,\
    error_valid_data_for_entry,\
    error_limit_send_sms,\
    error_time_send_sms,\
    error_not_exist_user_with_data,\
    good_restore_password
import traceback
import hashlib
from coffee_app.api_mobile_barista.redis.redis_sms import get_count_code,\
    check_phone,\
    get_time_,\
    set_phone,\
    add_count_code,\
    get_time_code

from coffee_app.config_app import user,\
    port,\
    password,\
    database,\
    host,\
    COUNT_SMS


async def is_exist_shop(id_shop, conn):
    select = '''
        SELECT id_coffee_shop
        FROM coffee_shop
        WHERE id_coffee_shop = {}
    '''.format(id_shop)
    row = await conn.fetchrow(select)
    if row:
        return True
    return False


async def get_data_shop(id_shop, conn):
    select = '''
        SELECT id_coffee_shop, title, description, ST_x(ST_Transform( location, 4326)),
          ST_y(ST_Transform( location, 4326)), working_day,time_start,time_end,path_img, address, code_for_entry
        FROM coffee_shop
        WHERE id_coffee_shop = {}
    '''.format(id_shop)
    row = await conn.fetchrow(select)
    return {
        'id_shop': row[0],
        'title': row[1],
        'description': row[2],
        'latlon': {
            'latitude': row[3],
            'longitude': row[4],
        },
        'working_day': row[5],
        'time_start': str(row[6]),
        'time_end': str(row[7]),
        'path_img': row[8],
        'address': row[9],
        'code_for_join': row[10]
    }


async def get_shops_network(id_network,
                            conn):
    select = '''
        SELECT id_coffee_shop, title, description, ST_x(ST_Transform( location, 4326)),
          ST_y(ST_Transform( location, 4326)), working_day,time_start,time_end,path_img, address
        FROM coffee_shop
        WHERE ref_id_coffee_shop_network = {}
    '''.format(id_network)
    return [
        {
            'id_shop': row[0],
            'title': row[1],
            'description': row[2],
            'latlon': {
                'latitude': row[3],
                'longitude': row[4],
            },
            'working_day': row[5],
            'time_start': str(row[6]),
            'time_end': str(row[7]),
            'path_img': row[8],
            'address': row[9]
        }
        for row in await conn.fetch(select)
    ]


async def entry_user(login_user,
                     password_user):
    conn = await asyncpg.connect(user=user,
                                 database=database,
                                 password=password,
                                 host=host,
                                 port=port)
    select = '''
        SELECT id_worker, login, password_hash, full_name, phone, ref_id_coffee_shop, ref_id_coffee_shop_network
        FROM worker
        WHERE login = '{}'
        AND password_hash = '{}'
    '''.format(login_user, password_user)
    if conn:
        try:
            row = await conn.fetchrow(select)
            if row:
                if row[1] == login_user and row[2] == password_user:
                    data = {
                        'user': {
                            'id_user': row[0],
                            'login': row[1],
                            'full_name': row[3],
                            'phone': row[4],
                            'id_shop': row[5],
                            'id_network': row[6]
                        }
                    }
                    shop = {}
                    if await is_exist_shop(row[5], conn):
                        shop = await get_data_shop(row[5], conn)
                    data['shop'] = shop
                    try:
                        await conn.close()
                    except:
                        pass
                    return {
                        'code': 1,
                        'message': good_entry,
                        'data': data
                    }

            try:
                await conn.close()
            except:
                pass
            return {
                'code': 1,
                'message': error_valid_data_for_entry,
                'data': {}
            }
        except:
            traceback.print_exc()
            try:
                await conn.close()
            except:
                pass
            return {
                'code': 0,
                'message': error_in_select,
                'data': {}
            }
    try:
        await conn.close()
    except:
        pass
    return {
        'code': 0,
        'message': error_in_connect,
        'data': {}
    }


async def restore_password_user(phone = None, login = None):
    def generate_code():
        return ''.join([str(random.randint(0, 9)) for _ in range(0, 6)])

    def create_hash_password(password):
        return hashlib.sha256(bytes(str(password), 'utf-8')).hexdigest()

    conn = await asyncpg.connect(user=user,
                                 database=database,
                                 password=password,
                                 host=host,
                                 port=port)
    select = '''
            SELECT id_worker, phone, login, full_name
            FROM worker
            WHERE phone = '{}' OR login = '{}'
        '''.format(phone, login)
    row = await conn.fetchrow(select)
    if row:
        if row[1] == phone or row[2] == login:
            if check_phone(phone):
                if get_count_code(phone) < COUNT_SMS:
                    # code =  generate_code()  # функция генерации кода для смс
                    code = '000000'  # временный код
                    hash_ = create_hash_password(code)
                    update = '''
                                UPDATE worker
                                SET password_hash = '{}'
                                WHERE id_worker = {}
                            '''.format(hash_,
                                       row[0])
                    async with conn.transaction():
                        await conn.execute(update)
                    set_phone(row[1])
                    add_count_code(row[1])
                    '''send_sms(phone,
                                     text_sms_message.format(code), 
                                     title_sms)
                            '''
                    await conn.close()
                    return {
                        'code': 1,
                        'message': good_restore_password,
                        'data': {
                            'left_time': get_time_(phone),
                            'left_sms': (COUNT_SMS - get_count_code(phone)),
                            }
                        }
                await conn.close()
                return {
                    'code': 1,
                    'message': error_limit_send_sms,
                    'data': {
                        'left_time': get_time_code(phone),
                        'left_sms': -1,
                    }
                }
            await conn.close()
            return {
                'code': 1,
                'message': error_time_send_sms,
                'data': {
                    'left_time': get_time_(phone),
                    'left_sms': (COUNT_SMS - get_count_code(phone)),
                }
            }
    await conn.close()
    return {
        'code': 1,
        'message': error_not_exist_user_with_data,
        'data': {}

    }

