from coffee_app.api_mobile_barista.api import api_entry, \
    api_restore_password, \
    api_logout
from coffee_app.api_mobile_barista.helpers.generators import data_to_token
from coffee_app.api_mobile_barista.helpers.validators import token_to_data
from aiohttp import web
import traceback
from coffee_app.config_app import CODE_ERROR,\
    role_worker_network
from coffee_app.api_mobile_barista.answers import error_unknown,\
    error_arguments,\
    good_logout
from coffee_app.api_mobile_barista.entry.db_entry import entry_user,\
    restore_password_user
from coffee_app.api_mobile_barista.redis.redb import set_client,\
    remove_user


async def entry_barista(request):
    try:
        di = await request.json()
        print(di)
        login = di['login']
        password = di['password']
        res = await entry_user(login, password)
        if res['message']['code'] > 0:
            token = data_to_token(res['data']['user'])
            set_client(res['data']['user']['id_user'], token, role_worker_network)
            return web.json_response(res, headers={'Token': token})
        return web.json_response(res, status=CODE_ERROR-res['message']['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_arguments,
            'data': {}
        }, status=CODE_ERROR-error_arguments['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=CODE_ERROR - error_unknown['code'])


async def restore_password_barista(request):
    try:
        di = dict(await request.json())
        try:
            phone = di['phone']
        except KeyError:
            phone = None
        try:
            login = di['login']
        except:
            login = None
        if phone or login:
            data = await restore_password_user(phone,login)
            if data['message']['code'] > 0:
                web.json_response(data)
            return web.json_response(data,
                                     status=CODE_ERROR-data['message']['code'])

        return web.json_response({
            'code': 1,
            'message': error_arguments,
            'data': {}
        }, status=CODE_ERROR - error_arguments['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=CODE_ERROR - error_unknown['code'])


async def logout_user(request):
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good:
            remove_user(data['id_user'], role_worker_network)
            return web.json_response({
                'code': 1,
                'message': good_logout,
                'data': {}
            })

        return web.json_response({
            'code': 1,
            'message': error_arguments,
            'data': {}
        }, status=CODE_ERROR - error_arguments['code'])
    except:
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=CODE_ERROR - error_unknown['code'])


