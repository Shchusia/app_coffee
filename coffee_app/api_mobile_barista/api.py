from coffee_app.config_app import domain_api_mobile_worker

api_entry = domain_api_mobile_worker + '/entry'
api_restore_password = domain_api_mobile_worker + '/restore_password'
api_logout = domain_api_mobile_worker + '/logout'

api_get_shops = domain_api_mobile_worker + '/get_shops'
api_move_to_shops = domain_api_mobile_worker + '/barista/move/to/shop'
api_get_menu_network = domain_api_mobile_worker + '/network/menu'

api_set_menu_to_shop = domain_api_mobile_worker + '/shop/menu/set'

