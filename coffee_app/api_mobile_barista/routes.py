from coffee_app.api_mobile_barista.entry.api_entry import entry_barista,\
    restore_password_barista,\
    logout_user
from coffee_app.api_mobile_barista.api import api_entry,\
    api_restore_password,\
    api_logout

from coffee_app.api_mobile_barista.shop.api_shop import get_list_shops,\
    move_barista, \
    get_menu_network, \
    set_consumable_to_shop
from coffee_app.api_mobile_barista.api import api_get_menu_network, \
    api_set_menu_to_shop, \
    api_get_shops, \
    api_move_to_shops


routes = [
    ('POST', api_entry, entry_barista, 'entry'),
    ('POST', api_restore_password, restore_password_barista, 'restore_password_barista'),
    ('GET', api_logout, logout_user, 'logout_user'),
    ('GET', api_get_shops, get_list_shops, 'get_list_shops'),
    ('POST', api_move_to_shops, move_barista, 'move_barista'),
    ('GET', api_get_menu_network, get_menu_network, 'get_menu_network'),
    ('POST', api_set_menu_to_shop, set_consumable_to_shop, 'set_consumable_to_shop')
]
