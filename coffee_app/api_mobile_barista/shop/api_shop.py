from coffee_app.api_mobile_barista.api import api_entry, \
    api_restore_password, \
    api_logout,\
    api_get_shops,\
    api_move_to_shops,\
    api_get_menu_network,\
    api_set_menu_to_shop
from coffee_app.api_mobile_barista.helpers.validators import token_to_data

from aiohttp import web
import traceback
from coffee_app.config_app import CODE_ERROR,\
    role_worker_network
from coffee_app.api_mobile_barista.answers import error_unknown,\
    error_arguments,\
    good_logout,\
    error_not_exist_your_shop
from coffee_app.api_mobile_barista.entry.db_entry import entry_user
from coffee_app.api_mobile_barista.redis.redb import set_client,\
    remove_user
from coffee_app.api_mobile_barista.shop.db_shop import get_shops_network,\
    move_barista_to_shop,\
    db_get_menu_network,\
    is_work_shop_in_network,\
    get_consumable_network,\
    db_set_quantity_to_shop


async def get_list_shops(request):
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        data = await get_shops_network(data['id_network'])
        if data['message']['code'] > 0:
            return web.json_response(data)
        return web.json_response(data,status=CODE_ERROR-data['message']['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=CODE_ERROR - error_unknown['code'])


async def move_barista(request):
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        di = dict(await request.json())
        code = di['code']
        id_new_shop = di['id_shop']
        data = await move_barista_to_shop(id_new_shop,
                                          data['id_user'],
                                          data['id_network'],
                                          code)
        if data['message']['code']:
            return web.json_response(data)
        return web.json_response(data,
                                 status=CODE_ERROR-data['message']['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_arguments,
            'data': {}
        }, status=CODE_ERROR - error_arguments['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=CODE_ERROR - error_unknown['code'])


async def get_menu_network(request):
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_work_shop_in_network(data['id_shop'],
                                   data['id_network']):
            data = await db_get_menu_network(data['id_network'],
                                             data['id_shop'])
            if data['message']['code']:
                return web.json_response(data)
            return web.json_response(data,
                                     status=CODE_ERROR-data['message']['code'])
        return web.json_response({
            'code': 1,
            'message': error_not_exist_your_shop,
            'data': {}
        }, status=CODE_ERROR-error_not_exist_your_shop['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_arguments,
            'data': {}
        }, status=CODE_ERROR - error_arguments['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=CODE_ERROR - error_unknown['code'])


async def set_consumable_to_shop(request):
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        list_products = (await request.json()).getall('products')
        if is_work_shop_in_network(data['id_shop'],
                                   data['id_network']):
            list_for_set = await get_consumable_network(data['id_network'], list_products)

            data = await db_set_quantity_to_shop(list_for_set,
                                                 data['id_shop'])

            if data['message']['code']:
                return web.json_response(data)
            return web.json_response(data,
                                     status=CODE_ERROR-data['message']['code'])
        return web.json_response({
            'code': 1,
            'message': error_not_exist_your_shop,
            'data': {}
        }, status=CODE_ERROR-error_not_exist_your_shop['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_arguments,
            'data': {}
        }, status=CODE_ERROR - error_arguments['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=CODE_ERROR - error_unknown['code'])
