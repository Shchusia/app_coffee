import asyncpg
import random
from datetime import datetime, timedelta
from coffee_app.api_mobile_barista.answers import error_in_connect, \
    error_in_select, \
    good_get_shops, \
    error_not_shop_in_your_network, \
    error_cod_is_ald, \
    good_move_to_shop, \
    good_get_menu
import traceback
from coffee_app.config_app import user, \
    port, \
    password, \
    database, \
    host, \
    COUNT_SMS
from coffee_app.api_mobile_barista.entry.db_entry import get_data_shop


async def get_shops_network(id_network):
    conn = await asyncpg.connect(user=user,
                                 database=database,
                                 password=password,
                                 host=host,
                                 port=port)
    if conn:
        try:
            select = '''
                SELECT id_coffee_shop, title, description, ST_x(ST_Transform( location, 4326)),
                  ST_y(ST_Transform( location, 4326)), working_day,time_start,time_end,path_img, address
                FROM coffee_shop
                WHERE ref_id_coffee_shop_network = {}
            '''.format(id_network)
            res = [
                {
                    'id_shop': row[0],
                    'title': row[1],
                    'description': row[2],
                    'latlon': {
                        'latitude': row[3],
                        'longitude': row[4],
                    },
                    'working_day': row[5],
                    'time_start': str(row[6]),
                    'time_end': str(row[7]),
                    'path_img': row[8],
                    'address': row[9]
                }
                for row in await conn.fetch(select)
            ]
            await conn.close()
            return {
                'code': 1,
                'message': good_get_shops,
                'data': {
                    'shops': res
                }
            }
        except:
            await conn.close()
            traceback.print_exc()
            return {
                'code': 0,
                'message': error_in_select,
                'data': {}
            }
    await conn.close()
    return {
        'code': 0,
        'message': error_in_connect,
        'data': {}
    }


async def is_exist_shop_in_network(id_shop, id_network, id_user, conn):
    select = '''
        SELECT w.id_worker
        FROM worker w,  coffee_shop cs 
        WHERE cs.ref_id_coffee_shop_network = {}
        AND w.ref_id_coffee_shop_network = {}
        AND cs.id_coffee_shop = {}
        AND w.id_worker = {}
    '''.format(id_network,
               id_network,
               id_shop,
               id_user)
    row = await conn.fetchrow(select)
    if row:
        if row[0] == id_user:
            return True
    return False


async def is_exist_code(id_shop, code, conn):
    select = '''
        SELECT id_coffee_shop
        FROM coffee_shop 
        WHERE id_coffee_shop = {}
        AND code_for_entry = '{}'
    '''.format(id_shop, code)
    row = await conn.fetchrow(select)
    if row:
        if row[0] == id_shop:
            return True
    return False


async def get_data_user(conn, id_user):
    select = '''
            SELECT id_worker, login, password_hash, full_name, phone, ref_id_coffee_shop, ref_id_coffee_shop_network
            FROM worker
            WHERE id_worker = {}
        '''.format(id_user)
    row = await conn.fetchrow(select)
    return {
        'id_user': row[0],
        'login': row[1],
        'full_name': row[3],
        'phone': row[4],
        'id_shop': row[5],
        'id_network': row[6]
    }


async def move_barista_to_shop(id_shop,
                               id_barista,
                               id_network,
                               code):
    conn = await asyncpg.connect(user=user,
                                 database=database,
                                 password=password,
                                 host=host,
                                 port=port)
    if conn:
        try:
            if is_exist_shop_in_network(id_shop,
                                        id_network,
                                        id_barista,
                                        conn):
                if is_exist_code(id_shop, code, conn):
                    select = '''
                        UPDATE worker
                        SET ref_id_coffee_shop = {}
                        WHERE id_worker = {}
                    '''.format(id_shop,
                               id_barista)
                    await conn.execute(select)

                    data = {
                        'code': 1,
                        'message': good_move_to_shop,
                        'data': {
                            'shop': get_data_shop(id_shop, conn),
                            'user': await get_data_user(conn, id_barista)
                        }
                    }
                    await conn.close()
                    return data
                await conn.close()
                return {
                    'code': 1,
                    'message': error_cod_is_ald,
                    'data': {}
                }
            await conn.close()
            return {
                'code': 1,
                'message': error_not_shop_in_your_network,
                'data': {}
            }
        except:
            await conn.close()
            traceback.print_exc()
            return {
                'code': 0,
                'message': error_in_select,
                'data': {}
            }
    await conn.close()
    return {
        'code': 0,
        'message': error_in_connect,
        'data': {}
    }


async def db_get_concrete(id_product, cur):
    select = '''
        SELECT id_concrete_product_info, sub_title, price, volume, dop_info
        FROM concrete_product_info
        WHERE  ref_id_product = {}
    '''.format(id_product)

    return [{
        'id_concrete': r[0],
        'sub_title': r[1],
        'price': str(r[2]) if r[2] else '',
        'volume': str(r[3]) if r[3] else '',
        'dop_info': r[4] if r[4] else ''

    } for r in await cur.fetch(select)]


async def db_get_menu_network(id_network, id_shop):
    conn = await asyncpg.connect(user=user,
                                 database=database,
                                 password=password,
                                 host=host,
                                 port=port)
    if conn:
        try:
            select = '''
                   SELECT c.id_category, c.title_category, c.ref_id_category_type,ct.type_category
                   FROM category c, coffee_shop_network csn,category_type ct
                   WHERE c.ref_id_shop_coffee_network = csn.id_coffee_shop_network
                   AND csn.id_coffee_shop_network = {}
                   AND c.ref_id_category_type != 4
                   AND ct.id_category_type = c.ref_id_category_type
               '''.format(id_network)
            rows = await conn.fetch(select)
            data = []
            for r in rows:
                select_get_product = '''
                      SELECT p.id_product, p.main_title, p.path_to_img, p.description, p.other_option, p.topings
                      FROM product p
                      WHERE p.ref_id_category = {}
                      ORDER BY p.id_product DESC
                      '''.format(r[0])
                tmp_res = []
                for r_ in await conn.fetch(select_get_product):
                    tmp_res.append({
                        'id_product': r_[0],
                        'title': r_[1],
                        'path_img': r_[2],
                        'description': r_[3],
                        'other_option': r_[4],
                        'toppings': r_[5],
                        'concrete_products': await db_get_concrete(r_[0], conn)})

                data.append({
                    'id_category':r[0],
                    'title_category': r[1],
                    'id_type_category': r[2],
                    'type_category': r[3],
                    'products': tmp_res,

                })
            menu_shop = await get_menu_shop(id_shop, conn)
            await conn.close()
            return {
                'code': 1,
                'message': good_get_menu,
                'data': {
                    'products': data,
                    'menu_shop': menu_shop
                }
            }

        except:
            await conn.close()
            traceback.print_exc()
            return {
                'code': 0,
                'message': error_in_select,
                'data': {}
            }
    await conn.close()
    return {
        'code': 0,
        'message': error_in_connect,
        'data': {}
    }


async def is_work_shop_in_network(id_shop, id_network):
    conn = await asyncpg.connect(user=user,
                                 database=database,
                                 password=password,
                                 host=host,
                                 port=port)
    if conn:
        try:
            select = '''
                SELECT id_coffee_shop
                FROM coffee_shop 
                WHERE id_coffee_shop = {}
                AND ref_id_coffee_shop_network = {}
            '''.format(id_shop, id_network)
            await conn.close()
            row = await conn.fetchrow(select)
            if row:
                if int(row[0]) == int(id_shop):
                    return True
            return False
        except:
            await conn.close()
            return False
    await conn.close()
    return False


async def get_menu_shop(id_shop, conn):
    select = '''
        SELECT ref_id_product
        FROM coffee_shop_to_product_menu
        WHERE ref_id_coffee_shop = {}
    '''.format(id_shop)
    return await conn.fetch(select)


async def get_consumable_shop_quantity(id_shop, conn):
    select = '''
        SELECT ref_id_product, quantity
        FROM coffee_shop_to_product_accounting
        WHERE ref_id_coffee_shop = {}
    '''.format(id_shop)
    return {
        r[0]: r[1]
        for r in await conn.fetch(select)
    }


async def get_consumable_shop(id_shop):
    conn = await asyncpg.connect(user=user,
                                 database=database,
                                 password=password,
                                 host=host,
                                 port=port)
    if conn:
        try:
            select = '''
                SELECT c.id_category, c.title_category, c.ref_id_category_type,ct.type_category
                FROM category c, coffee_shop_network csn,category_type ct
                WHERE c.ref_id_shop_coffee_network = csn.id_coffee_shop_network
                AND csn.id_coffee_shop_network = {}
                AND c.ref_id_category_type = 4
                AND ct.id_category_type = c.ref_id_category_type
            '''.format(id_shop)
            rows = await conn.fetch(select)
            data = {}
            consumble_product = await get_consumable_shop_quantity(id_shop, conn)
            for r in rows:
                select_get_product = '''
                                  SELECT p.id_product, p.main_title, p.path_to_img, p.description, p.other_option, p.topings
                                  FROM product p
                                  WHERE p.ref_id_category = {}
                                  ORDER BY p.id_product DESC
                                  '''.format(r[0])
                tmp_res = []
                for r_ in await conn.fetch(select_get_product):
                    tmp_res.append({
                        'id_product': r_[0],
                        'title': r_[1],
                        'path_img': r_[2],
                        'description': r_[3],
                        'other_option': r_[4],
                        'toppings': r_[5],
                        'concrete_products': await db_get_concrete_consumble(r_[0],
                                                                             conn,
                                                                             consumble_product)})
                data[r[0]] = {
                    'title_category': r[1],
                    'id_type_category': r[2],
                    'type_category': r[3],
                    'products': tmp_res,

                }


        except:
            await conn.close()
            traceback.print_exc()
            return {
                'code': 0,
                'message': error_in_select,
                'data': {}
            }
    await conn.close()
    return {
        'code': 0,
        'message': error_in_connect,
        'data': {}
    }


async def db_get_concrete_consumble(id_product, cur, consumble):
    select = '''
        SELECT id_concrete_product_info, sub_title, price, volume, dop_info
        FROM concrete_product_info
        WHERE  ref_id_product = {}
    '''.format(id_product)
    return [{
        'id_concrete': r[0],
        'quantity': consumble[r[0]] if consumble.get([r[0]], False) else 0
    } for r in await cur.fetch(select)]


async def db_set_quantity_to_shop(data, id_shop):
    conn = await asyncpg.connect(user=user,
                                 database=database,
                                 password=password,
                                 host=host,
                                 port=port)
    if conn:
        try:
            select_insert = '''
                INSERT INTO coffee_shop_to_product_accounting (ref_id_product_, ref_id_coffee_shop_, quantity)
                VALUES ({}, {}, {}) ON CONFLICT ON CONSTRAINT coffee_shop_to_product_accounting_pkey DO UPDATE
                SET quantity = {}
            '''
            for d in data:
                await conn.execute(select_insert.format(d['id_product'],
                                                        id_shop,
                                                        abs(int(d['quantity'])),
                                                        abs(int(d['quantity']))))
            await conn.close()
            return await get_consumable_shop(id_shop)
        except:
            await conn.close()
            traceback.print_exc()
            return {
                'code': 0,
                'message': error_in_select,
                'data': {}
            }
    await conn.close()
    return {
        'code': 0,
        'message': error_in_connect,
        'data': {}
    }


async def get_consumable_network(id_network, data):
    conn = await asyncpg.connect(user=user,
                                 database=database,
                                 password=password,
                                 host=host,
                                 port=port)
    if conn:
        try:
            select = '''
                  SELECT c.id_category, c.title_category, c.ref_id_category_type,ct.type_category
                  FROM category c, coffee_shop_network csn,category_type ct
                  WHERE c.ref_id_shop_coffee_network = csn.id_coffee_shop_network
                  AND csn.id_coffee_shop_network = {}
                  AND c.ref_id_category_type != 4
                  AND ct.id_category_type = c.ref_id_category_type
            '''.format(id_network)
            data_b = set(await conn.fetch(select))
            res = set([int(d['id_product']) for d in data])
            await conn.close()
            res = list(data_b and res)
            answer = []
            for r in res:
                for d in data:
                    if int(d['id_product']) == int(r):
                        answer.append(d)
                        break

            return answer
        except:
            await conn.close()
            traceback.print_exc()
            return {
                'code': 0,
                'message': error_in_select,
                'data': {}
            }
    await conn.close()
    return {
        'code': 0,
        'message': error_in_connect,
        'data': {}
    }
