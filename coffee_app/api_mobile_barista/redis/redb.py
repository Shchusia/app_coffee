import traceback
from coffee_app.config_app import r

ttl_phone_in_redis = 300


def set_client(id_user, session_key, role):
    r.set('{}_{}'.format(role, id_user), session_key)


def check_valid_data(id_user, session_key, role):
    try:
        key = r.get('{}_{}'.format(role, id_user))
        if key.decode('utf-8') == session_key:
                return True
    except:
        traceback.print_exc()
    return False


def sign_out(id_user, role):
    r.delete('{}_{}'.format(role, id_user))


def remove_user(id_user, role):
    r.delete('{}_{}'.format(role, id_user))
