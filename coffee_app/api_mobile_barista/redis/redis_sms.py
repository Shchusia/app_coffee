from coffee_app.config_app import r_sms as rp, \
    r_time as r
import datetime


def set_phone(phone, ttl=300):
    r.set(phone, 'cool')
    r.expire(phone, ttl)


def check_phone(phone):
    try:
        is_exist = r.get(phone)
        # print(is_exist)
        if is_exist is None:
            return True
        return False
    except:
        return True


def get_time_(phone):
    try:
        return r.ttl(phone)
    except:
        return 0


def get_time_code(phone):
    try:
        return rp.ttl(phone)
    except:
        return 0


def get_time_for_next_day():
    d = datetime.datetime.today()
    try:

        next_day = datetime.datetime.today().replace(day=d.day + 1)
    except ValueError:

        try:
            next_day = datetime.datetime.today().replace(month=d.month + 1, day=1)
        except ValueError:
            next_day = datetime.datetime.today().replace(year=d.year + 1, month=1, day=1)
    new_day = datetime.datetime(year=next_day.year, month=next_day.month, day=next_day.day)

    tn = new_day.timestamp()
    cd = d.timestamp()
    return int(tn) - int(cd)


def get_count_code(phone):
    try:
        temp = rp.get(phone)
        if temp:
            return int(temp.decode('utf-8'))
        return 0
    except:
        return 0


def add_count_code(phone):
    try:
        temp = rp.get(phone)
        if temp:
            c = int(temp.decode('utf-8')) + 1
            rp.set(phone, c)
            rp.expire(phone, get_time_for_next_day())  # если надо ттл
        else:
            rp.set(phone, 1)
            rp.expire(phone, get_time_for_next_day())  # если надо ттл
    except:
        rp.set(phone, 1)
        rp.expire(phone, get_time_for_next_day())  # если надо ттл
