from re import match
import traceback
from coffee_app.config_app import secret_word
from coffee_app.config_app import algorithm_encode
import jwt


def check_email(email):
    if not match("[\\w._%+-|]+@[\\w0-9.-]+\\.[A-Za-z]{2,6}", email):
        return False
    return True


def is_valid_phone(phone):
    def phone_to_easy_phone(phone_):
        '''+995322ХХХХХХ
        +380979491074
        +79215789560
        '''
        answer = ''
        for p in phone_:
            if p.isnumeric():
                answer += p
        return answer

    phone = phone_to_easy_phone(phone)
    if len(phone) == 11 or len(phone) == 12:
        try:
            _ = int(phone)
            return True
        except ValueError:
            return False


def token_to_data(token):
    '''
    преобразовывает токен в данные пользователя
    :param token: токен который выдавался пользователю
    :return:json с данными пользователя
    '''
    try:
        return True, jwt.decode(token, secret_word, algorithms=[algorithm_encode])
    except jwt.exceptions.DecodeError:
        traceback.print_exc()
        return False, {}


def check_input_parameter_in_json(swap_data,
                                  input_data):
    '''
    проверка данных пользователя присланных в запросе
    :param swap_data:
    :param input_data:
    :return:
    '''
    must_key = swap_data.keys()
    exist_key = []
    status_check = True
    for key in must_key:
        swap_data[key] = input_data.get(key)
        if not swap_data[key]:
            exist_key.append(key)
            status_check = False

    return status_check, exist_key, swap_data


def check_len_parameters(check_key, input_data):
    '''
    проверка длины присланных данных
    :param check_key:
    :param input_data:
    :return:
    '''
    # {key:len}
    bed_keys = []
    status = True
    for key in check_key.keys():
        if len(input_data[key]) > check_key[key]:
            status = False
            bed_keys.append(key)
    return status, bed_keys


def check_on_int_parameters(check_key,
                            input_data):
    '''
    проверка на типы
    :param check_key:
    :param input_data:
    :return:
    '''
    bed_keys = []
    status = True
    for key in check_key:
        if type(input_data[key]) != int:
            status = False
            bed_keys.append(key)
    return status, bed_keys
