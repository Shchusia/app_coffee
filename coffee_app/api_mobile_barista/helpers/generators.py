import jwt
import random
import hashlib
import traceback
from coffee_app.config_app import secret_word
from coffee_app.config_app import algorithm_encode


def data_to_token(data):
    '''
    формирует токен по данным пользователя json
    :param data: данные пользователя имя пароль id
    :return: возвращает токен для пользователя
    '''

    return jwt.encode(data, secret_word, algorithm=algorithm_encode).decode()


def generate_code():
    return ''.join([str(random.randint(0, 9)) for _ in range(0, 6)])


def create_hash_password(password):
    return hashlib.sha256(bytes(str(password), 'utf-8')).hexdigest()


if __name__ == '__main__':
    print(create_hash_password('ddddd'))