from aiohttp import web
import jwt
import traceback
from coffee_app.config_app import MAX_LENGTH_FILE,\
    secret_word,\
    algorithm_encode,\
    CODE_ERROR,\
    role_worker_network,\
    user,\
    password,\
    database,\
    host,\
    port
from coffee_app.api_mobile_barista.answers import error_arguments,\
    error_not_valid_token,\
    error_unknown
from coffee_app.api_mobile_barista.redis.redb import check_valid_data
import asyncio
import asyncpg
from coffee_app.api_mobile_barista.routes import routes,\
    api_entry, \
    api_restore_password


def data_to_token(data):
    '''
    формирует токен по данным пользователя json
    :param data: данные пользователя имя пароль id
    :return: возвращает токен для пользователя
    '''

    return jwt.encode(data, secret_word, algorithm=algorithm_encode).decode()


def token_to_data(token):
    '''
    преобразовывает токен в данные пользователя
    :param token: токен который выдавался пользователю
    :return:json с данными пользователя
    '''
    try:
        return True, jwt.decode(token, secret_word, algorithms=[algorithm_encode])
    except jwt.exceptions.DecodeError:
        traceback.print_exc()
        return False, {}


@web.middleware
async def middleware_(request, handler):
    print(request.path)
    print(request.headers)
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and check_valid_data(data['id_user'], token, role_worker_network):
            response = await handler(request)
            return response

        return web.json_response({
            'code': 1,
            'message': error_not_valid_token,
            'data': {},
        }, status=CODE_ERROR-error_not_valid_token['code'])
    except KeyError:
        traceback.print_exc()
        # print(request.path)
        # int(api_entry)
        # print(api_restore_password)
        # print(request.path == api_entry)
        # print(request.path == api_restore_password)
        if request.path == api_entry or request.path == api_restore_password:
            return await handler(request)
        return web.json_response({
            'code': 1,
            'message': error_arguments,
            'data': {}
        }, status=CODE_ERROR-error_arguments['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': error_unknown,
            'data': {}
        }, status=CODE_ERROR-error_unknown['code'])


async def init_app(loop):
    # инициализация приложения с декоратором методов
    app = web.Application(middlewares=[middleware_],
                          client_max_size=MAX_LENGTH_FILE)
                          # loop=loop)

    app['pool'] = await asyncpg.create_pool(user=user,
                                            database=database,
                                            password=password,
                                            host=host,
                                            port=port)

    # инициализация всех роутов мобильного апи
    for route in routes:
        app.router.add_route(route[0],
                             route[1],
                             route[2],
                             name=route[3])

    return app



# loop = asyncio.get_event_loop()
#
# app = web.Application(client_max_size=MAX_LENGTH_FILE,
#                       middlewares=[middleware_],
#                       loop=loop)
#
# import api_mobile_barista.entry.api_entry
# import api_mobile_barista.shop.api_shop
loop_ = asyncio.get_event_loop()
app_ = loop_.run_until_complete(init_app(loop_))
# app_ = init_app(loop_)
if __name__ == '__main__':


    web.run_app(app_, port=5007)
    # web.run_app(app, port=5050)
