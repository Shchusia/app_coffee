from coffee_app.web_admin.__init__ import app

from flask import render_template,\
    jsonify,\
    request,\
    redirect, \
    url_for
import hashlib
import requests
from coffee_app.web_admin.urls.web.web_workers import url_worker_create,\
    url_worker_remove,\
    url_workers,\
    url_worker_move_place
from coffee_app.web_admin.urls.api.api_worker import api_new_work_place,\
    api_delete_worker,\
    api_workers,\
    api_create_worker
from coffee_app.web_admin.answer_web_server import status_code_not_200


def workers():
    res = requests.get(api_workers, headers={'Token': request.cookies.get('token')})
    if res.status_code == 200:
        if res.json()['message']['code'] > 0:
            return render_template('admin_network/workers.html',
                                   workers=res.json()['data']['workers'],
                                   shops=res.json()['data']['shops'],
                                   url_move=url_worker_move_place)
        else:
            return render_template('admin_network/workers.html',
                                   message=res.json()['message']['body'])

    return render_template('admin_network/workers.html',
                           message=status_code_not_200)


def remove_worker(id_worker):
    requests.get(api_delete_worker,
                 headers={'Token': request.cookies.get('token'),
                          'Id-Worker': id_worker})
    return redirect(url_for('workers'))


def create_worker():
    def create_hash_password(password):
        return hashlib.sha256(bytes(str(password), 'utf-8')).hexdigest()
    di = dict(request.form)
    data = {
        'full_name': di["full_name"][0],
        'phone': di["phone"][0],
        'login': di["login"][0],
        'password': create_hash_password(di["password"][0]),
        'id_coffee_shop': di["coffee_shop"][0]
    }
    res = requests.post(api_create_worker,
                        json=data,
                        headers={'Token': request.cookies.get('token')}).json()
    if res['message']['code'] > 0:
        return redirect(url_for('workers'))
    message = res['message']['body']
    res = requests.get(api_workers, headers={'Token': request.cookies.get('token')})
    return render_template('admin_network/workers.html',
                           workers=res.json()['data']['workers'],
                           shops=res.json()['data']['shops'],
                           message=message)


def move_worker(id_worker, id_place):
    res = requests.get(api_new_work_place,
                       headers={'Token': request.cookies.get('token'),
                                'Id-Worker': id_worker,
                                'Id-Coffee-Shop': id_place}).json()
    return jsonify({})


app.add_url_rule(url_workers, 'workers', workers, methods=['GET'])
app.add_url_rule(url_worker_remove+'/<id_worker>', 'remove_worker', remove_worker,methods=['GET'])
app.add_url_rule(url_worker_create, 'create_worker', create_worker, methods=['POST'])
app.add_url_rule(url_worker_move_place + '/<id_worker>/<id_place>', 'move_worker', move_worker, methods=['GET'])
