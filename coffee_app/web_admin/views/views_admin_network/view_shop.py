from coffee_app.web_admin.__init__ import app

from flask import render_template,\
    request,\
    redirect, \
    url_for
import requests
from coffee_app.web_admin.urls.web.web_shop import url_create_shop,\
    url_shops,\
    url_update_gatherer,\
    url_ban_shop,\
    url_remove_shop
from coffee_app.web_admin.urls.api.api_admins_network import api_upload_photo,\
    api_share_gatherer_network
from coffee_app.web_admin.urls.api.api_shop import api_create_shops,\
    api_get_list_shops,\
    api_remove_shop,\
    api_ban_shop
from coffee_app.web_admin.answer_web_server import status_code_not_200


def shops():
    res = requests.get(api_get_list_shops,
                       headers={'Token': request.cookies.get('token')})
    if res.status_code == 200 and res.json()['message']['code'] > 0:
        return render_template('admin_network/shops.html',
                               shops=res.json()['data']['shops'],
                               gatherer=res.json()['data']['gatherer'])
    return render_template('admin_network/shops.html',
                           message=status_code_not_200)


def create_shop():
    di = dict(request.form)
    files = {'file': request.files.getlist("file")[0]}
    res = requests.post(api_upload_photo,
                        files=files,
                        headers={'Token': request.cookies.get('token')})
    # print(res.json())
    if res.status_code == 200 and res.json()['message']['code'] > 0:
        print(di)
        data = {
            'path_img': res.json()['data']['path_to_file'],
            'title': di['title'][0],
            'description': di['description'][0],
            'time_start': di['time_start'][0],
            'time_end': di['time_end'][0],
            'latitude': di['latitude'][0],
            'longitude': di['longitude'][0],
            'address': di['address'][0],
        }
        week = []
        for i in range(1, 8):
            if di.get('week'+str(i), False):
                week.append('True')
            else:
                week.append('False')
        data['week'] = week
        res = requests.post(api_create_shops,
                            json=data,
                            headers={'Token': request.cookies.get('token')})
        print(data)
        if res.status_code == 200 and res.json()['message']['code'] > 0:
            # print(data)
            # https://maps.googleapis.com/maps/api/geocode/json?latlng=49.993615462541136,36.23291015625&key=AIzaSyAtdcwr5Dojv-zdFs3IMs1Cl2vAFkpoj08&language=ru

            return redirect(url_for('shops'))
        else:
            res = requests.get(api_get_list_shops,
                               headers={'Token': request.cookies.get('token')})
            if res.status_code == 200 and res.json()['message']['code'] > 0:
                return render_template('admin_network/shops.html',
                                       shops=res.json()['data']['shops'],
                                       gatherer=res.json()['data']['gatherer'])
            return render_template('admin_network/shops.html',
                                   message=status_code_not_200)


def update_gatherer():
    di = dict(request.form)
    try:
        is_gatherer_shares = di['is_gatherer_shares'][0]
        is_gatherer_shares = True
    except KeyError:
        is_gatherer_shares = False
    quantity_gatherer = int(di['quantity_gatherer'][0])
    data = {
        'quantity_gatherer': quantity_gatherer,
        'is_gatherer_shares': is_gatherer_shares
    }
    requests.post(api_share_gatherer_network, json=data, headers={'Token': request.cookies.get('token')})
    return redirect(url_for('shops'))


def ban_shop(id_shop):
    requests.get(api_ban_shop.format(id_shop),headers={'Token': request.cookies.get('token')} )
    return redirect(url_for('shops'))


def remove_shop(id_shop):
    requests.get(api_remove_shop.format(id_shop), headers={'Token': request.cookies.get('token')})
    return redirect(url_for('shops'))


app.add_url_rule(url_shops, 'shops', shops, methods=['GET'])
app.add_url_rule(url_create_shop, 'create_shop', create_shop, methods=['POST'])
app.add_url_rule(url_update_gatherer, 'update_gatherer', update_gatherer, methods=['POST'])
app.add_url_rule(url_remove_shop + '/<id_shop>', 'remove_shop', remove_shop, methods=['GET'])
app.add_url_rule(url_ban_shop + '/<id_shop>', 'ban_shop', ban_shop, methods=['GET'])
