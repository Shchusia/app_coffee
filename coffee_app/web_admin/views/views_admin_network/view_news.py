from coffee_app.web_admin.__init__ import app

from flask import render_template,\
    request,\
    redirect, \
    url_for
import requests
from coffee_app.web_admin.answer_web_server import status_code_not_200
from coffee_app.web_admin.urls.web.web_news import url_create_news, \
    url_news, \
    url_news_remove
from coffee_app.web_admin.urls.api.api_news import api_delete_news,\
    api_list_news,\
    api_create_news
from coffee_app.web_admin.urls.api.api_admins_network import api_upload_photo


def news():
    res = requests.get(api_list_news, headers={'Token': request.cookies.get('token')})
    if res.status_code == 200 and res.json()['message']['code'] > 0:
        return render_template('admin_network/news.html',
                               news=res.json()['data']['news'])
    return render_template('admin_network/news.html',
                           message=status_code_not_200)


def create_news():
    di = dict(request.form)

    # print(request.files.getlist("file")[0].content_type)
    if request.files.getlist("file")[0].content_type == 'application/octet-stream':
        path_img = ''
    else:
        files = {'file': request.files.getlist("file")[0]}
        res = requests.post(api_upload_photo,
                            files=files,
                            headers={'Token': request.cookies.get('token')})
        path_img = res.json()['data']['path_to_file']
    # print(path_img)
    title = di['title'][0]
    text_news = di['text_news'][0]
    data = {
        'title_news': title,
        'text_news': text_news,
        'path_img': path_img
    }
    requests.post(api_create_news,
                  json=data,
                  headers={'Token': request.cookies.get('token')})
    return redirect(url_for('news'))


def remove_news(id_news):
    requests.get(api_delete_news, headers={'Token': request.cookies.get('token'),
                                           'Id-News': id_news})
    return redirect(url_for('news'))


app.add_url_rule(url_news, 'news', news, methods=['GET'])
app.add_url_rule(url_news_remove + '/<id_news>', 'remove_news', remove_news, methods=['GET'])
app.add_url_rule(url_create_news, 'create_news', create_news, methods=['POST'])
