from coffee_app.web_admin.__init__ import app

from flask import render_template,\
    request,\
    redirect, \
    url_for
import requests
from coffee_app.web_admin.answer_web_server import status_code_not_200
from coffee_app.web_admin.urls.web.web_category import url_categories, \
    url_create_category,\
    url_remove_category
from coffee_app.web_admin.urls.api.api_category import api_remove_category,\
    api_get_categories,\
    api_create_category


def categories():
    res = requests.get(api_get_categories,
                       headers={
                           'Token': request.cookies.get('token')
                       })
    if res.status_code == 200 and res.json()['message']['code'] > 0:
        return render_template('admin_network/categories.html', categories=res.json()['data']['categories'])
    else:
        return render_template('admin_network/categories.html',
                               message=status_code_not_200)


def create_category():
    di = dict(request.form)
    title = di['category'][0]
    type_ = di['type_category'][0]
    data = {
        'title_category': title,
        'type_category': type_
    }
    requests.post(api_create_category,
                  json=data,
                  headers={
                      'Token': request.cookies.get('token')
                  })
    return redirect(url_for('categories'))


def remove_category(id_category):
    requests.get(api_remove_category,
                 headers={
                     'Token': request.cookies.get('token'),
                     'Id-Category': id_category})
    return redirect(url_for('categories'))


app.add_url_rule(url_categories, 'categories', categories, methods=['GET'])
app.add_url_rule(url_create_category, 'create_category', create_category, methods=['POST'])
app.add_url_rule(url_remove_category + '/<id_category>', 'remove_category', remove_category, methods=['GET'])
