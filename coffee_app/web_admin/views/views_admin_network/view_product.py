from coffee_app.web_admin.__init__ import app
import traceback

from flask import render_template,\
    request,\
    redirect, \
    url_for
import requests
from coffee_app.web_admin.answer_web_server import status_code_not_200
from coffee_app.web_admin.urls.web.web_products import url_create_product,\
    url_list_products,\
    url_remove_product,\
    url_product_copy,\
    url_new_version_product,\
    url_global_products
from coffee_app.web_admin.urls.api.api_category import api_get_categories
from coffee_app.web_admin.urls.api.api_admins_network import api_upload_photo
from coffee_app.web_admin.urls.api.api_product import api_get_list_products,\
    api_create_product,\
    api_toppings,\
    api_product_remove,\
    api_copy_products,\
    api_new_product,\
    api_get_consumable,\
    api_get_list_products_new
from coffee_app.web_admin.urls.api.api_shop import api_get_list_shops


def list_products(id_coffee_shop):
    ress = requests.get(api_get_list_shops,
                        headers={'Token': request.cookies.get('token')})
    # Id-Coffee-Shop
    res = requests.get(api_get_list_products,
                       headers={
                           'Token': request.cookies.get('token'),
                           'Id-Coffee-Shop': id_coffee_shop
                       }).json()
    sum = 0
    for r in res['data']['products'].keys():
        sum += len(res['data']['products'][r]['products'])
    is_copy = False
    if sum == 0:
        is_copy = True
    return render_template('admin_network/products.html',
                           id_coffee_shop=id_coffee_shop,
                           is_copy=is_copy,
                           shops=ress.json()['data']['shops'],
                           products=res['data']['products'],
                           keys=list(res['data']['products'].keys()))


def new_product(id_coffee_shop):
    message = ''
    if request.method == 'POST':
        files = {'file': request.files.getlist("file")[0]}
        res = requests.post(api_upload_photo,
                            files=files,
                            headers={'Token': request.cookies.get('token')})
        # print(res.json())
        if res.status_code == 200 and res.json()['message']['code'] > 0:
            di = dict(request.form)
            # print(di)
            category = int(di['category'][0].split('_')[0])
            type_category = int(di['category'][0].split('_')[1])
            title_product = di['title'][0]
            description_product = di['description'][0]
            quantity = di['quantity'][0]
            price = di['price'][0]
            try:
                is_gatherer = di['is_gatherer'][0]
                is_gatherer = True
            except KeyError:
                is_gatherer = False
            try:
                is_to_swap = di['is_to_swap'][0]
                is_to_swap = True
            except KeyError:
                is_to_swap = False
            data_ = []
            try:

                list_typing = list(di['toppings'])
            except:
                traceback.print_exc()
                list_typing = []
            if type_category == 1:
                try:
                    data_ = [
                        {
                            'concrete_title': di['title_drink'][i],
                            'concrete_description': di['description_drink'][i],
                            'concrete_price': di['price_'][i],
                            'volume': di['volume'][i]
                        }
                        for i in range(0, len(di['description_drink']))
                    ]
                except:
                    data_ = []
            data = {
                'id_coffee_shop': id_coffee_shop,
                'title_product': title_product,
                'description_product': description_product,
                'quantity': quantity,
                'price': price,
                'id_category': category,
                'is_in_gatherer': is_gatherer,
                'is_to_swap': is_to_swap,
                'sub_product': data_,
                'list_typing': list_typing,
                'path_img':  res.json()['data']['path_to_file']
            }
            print(data)
            res = requests.post(api_create_product, json=data, headers={'Token': request.cookies.get('token')})
            if res.status_code == 200 and res.json()['message']['code'] > 0:
                pass
            else:
                message = status_code_not_200
        else:
            message = status_code_not_200

    res = requests.get(api_get_categories,
                       headers={
                           'Token': request.cookies.get('token')
                       })
    resp = requests.get(api_toppings,
                        headers={
                            'Token': request.cookies.get('token'),
                            'Id-Coffee-Shop': id_coffee_shop
                        }).json()
    return render_template('admin_network/product.html',
                           id_coffee_shop=id_coffee_shop,
                           categories=res.json()['data']['categories'],
                           toppings=resp['data']['toppings'],
                           message=message)


def remove_product(id_product,
                   id_coffee_shop):
    requests.get(api_product_remove, headers={
        'Token': request.cookies.get('token'),
        'Id-Product': id_product
    })
    return redirect(url_for('list_products',
                            id_coffee_shop=id_coffee_shop))


def product_copy(id_coffee_shop):
    di = dict(request.form)
    copy_from = di['copy_from'][0]
    res = requests.get(api_copy_products,
                       headers={
                           'Token': request.cookies.get('token'),
                           'From': copy_from,
                           'To': id_coffee_shop
                       })
    print(res.status_code)
    return redirect(url_for('list_products',
                            id_coffee_shop=id_coffee_shop))


def new_version_product():
    if request.method == 'POST':
        files = {'file': request.files.getlist("file")[0]}
        res = requests.post(api_upload_photo,
                            files=files,
                            headers={'Token': request.cookies.get('token')})
        # print(res.json())
        if res.status_code == 200 and res.json()['message']['code'] > 0:
            di = dict(request.form)
            # print(di)
            category = di['category'][0]
            product = {
                'path_img': res.json()['data']['path_to_file'],
                'description': di['description'][0],
                'title': di['title'][0],
                'category':  category.split('_')[0],
                'main_category':   category.split('_')[1],
            }
            del di['category']
            del di['description']
            del di['title']

            tmp = []
            try:
                for i, val in enumerate(di['value_option']):
                    tmp.append({
                        'name_option': di['name_option'][i],
                        'value_option': val
                    })

            except:
                pass
            print(di)
            product['options'] = tmp
            product['toppings'] = []
            if int(category.split('_')[1]) == 1:
                try:
                    product['toppings'] = di['toppings']
                    del di['toppings']
                except:
                    product['toppings'] = []
                concrete_product = []
                ind = 0
                for i, val in enumerate(di['sub_title']):
                    tmp_ = {
                        'sub_title': val,
                        'price': di['price'][i],
                        'volume': di['volume'][i],
                    }
                    while True:

                        if di.get('other_{}'.format(ind), False):
                            tmp_other = []
                            for j, val_o in enumerate(di['other_{}'.format(ind)]):
                                print(di['other_val_{}'.format(ind)])
                                print(j)
                                print(val_o)
                                tmp_other.append({
                                    'id_consumable': val_o,
                                    'quantity_consumable': di['other_val_{}'.format(ind)][j]
                                })
                            tmp_['consumable'] = tmp_other
                            break
                        else:
                            ind += 1
                    concrete_product.append(tmp_)
                    # print(tmp_)
                product['concrete_product'] = concrete_product

            elif int(category.split('_')[1]) == 2:
                # topping
                product['price'] = di['price'][0]
                product['volume'] = di['volume'][0]
                pass
            elif int(category.split('_')[1]) == 3:
                # eat
                product['price'] = di['price'][0]
                pass
            elif int(category.split('_')[1]) == 4:
                # other
                pass
            else:
                # error
                pass
            requests.post(api_new_product,
                          headers={'Token': request.cookies.get('token')},
                          json=product)

    res = requests.get(api_get_categories,
                       headers={
                           'Token': request.cookies.get('token')
                       })
    # print(res.json()['data']['categories'])
    resp = requests.get(api_toppings,
                        headers={
                            'Token': request.cookies.get('token')
                        }).json()
    cons = requests.get(api_get_consumable,
                        headers={
                            'Token': request.cookies.get('token')
                        }).json()
    return render_template('admin_network/new_product.html',
                           categories=res.json()['data']['categories'],
                           toppings=resp['data']['toppings'],
                           other=cons['data']['consumable'])


def global_products():
    res = requests.get(api_get_list_products_new,
                       headers={
                           'Token': request.cookies.get('token')
                       }).json()
    resp = requests.get(api_toppings,
                        headers={
                            'Token': request.cookies.get('token')
                        }).json()
    toppings = resp['data']['toppings']
    top = {
        r['id_topping']: r['title_topping']
        for r in toppings
    }
    cons = requests.get(api_get_consumable,
                        headers={
                            'Token': request.cookies.get('token')
                        }).json()['data']['consumable']
    other = {
        c['id_other']: c['title_other']
        for c in cons
    }

    return render_template('admin_network/products_new.html',
                           products=res['data']['products'],
                           keys=list(res['data']['products'].keys()),
                           toppings=top,
                           consumable=other)


app.add_url_rule(url_list_products + '/<id_coffee_shop>', 'list_products', list_products, methods=['GET'])
app.add_url_rule(url_create_product + '/<id_coffee_shop>', 'new_product', new_product, methods=['POST', 'GET'])
app.add_url_rule(url_remove_product + '/<id_product>/<id_coffee_shop>', 'remove_product', remove_product, methods=['GET'])
app.add_url_rule(url_product_copy + '/<id_coffee_shop>', 'copy_products', product_copy, methods=['POST'])
app.add_url_rule(url_new_version_product, 'new_version_product', new_version_product, methods=['GET', 'POST'])

app.add_url_rule(url_global_products, 'global_products', global_products, methods=['GET'])
