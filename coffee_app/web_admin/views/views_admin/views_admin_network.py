from coffee_app.web_admin.__init__ import app

from flask import render_template,\
    jsonify,\
    request,\
    redirect, \
    url_for
import hashlib
import requests
from coffee_app.web_admin.urls.web.web_admin_network import url_admins_network,\
    url_new_admin_network,\
    url_ban_unban_network,\
    url_get_list_network,\
    url_update_admin_network,\
    url_remove_network
from coffee_app.web_admin.urls.api.api_admins_network import api_upload_photo,\
    api_add_new_network,\
    api_ban_unban_network,\
    api_get_list_network,\
    api_remove_network
from coffee_app.config_app import base_url
from coffee_app.web_admin.answer_web_server import status_code_not_200


def admins_network():
    res = requests.get(api_get_list_network,
                       headers={'Token': request.cookies.get('token')})
    if res.status_code == 200 and res.json()['message']['code'] > 0:
        return render_template('admin_app/admins.html',
                               message='',
                               networks=res.json()['data']['networks'])
    return render_template('admin_app/admins.html',
                           message=status_code_not_200)


def create_admin_network():
    def create_hash_password(password):
        return hashlib.sha256(bytes(str(password), 'utf-8')).hexdigest()

    di = dict(request.form)
    files = {'file': request.files.getlist("file")[0]}
    res = requests.post(api_upload_photo, files=files,headers={'Token': request.cookies.get('token')})
    print(res.json())
    if res.status_code == 200 and res.json()['message']['code'] > 0:
        data = {
            'title': di['title'][0],
            'login': di['login'][0],
            'password': create_hash_password(di['password'][0]),
            'contacts': di['contacts'][0],
            'color': di['color'][0],
            'path_img': res.json()['data']['path_to_file']
        }
        res = requests.post(api_add_new_network,
                            json=data,
                            headers={'Token':request.cookies.get('token')})
        print(res.status_code)
        print(res.json())

    return redirect(url_for('admins_network'))


def ban_unban_network(id_network):
    res = requests.get(api_ban_unban_network,
                       headers={'Token': request.cookies.get('token'),
                                'id_network': id_network})
    return redirect(url_for('admins_network'))


def update_network(id_network):
    if request.method == 'POST':
        return redirect(url_for('admins_network'))
    res = requests.get(api_get_list_network,
                       headers={'Token': request.cookies.get('token')})
    network = None
    for i in res.json()['data']['networks']:
        if int(i['id_network']) == int(id_network):
            network = i
            break
    return render_template('admin_app/admins.html',
                           message='',
                           network=network,
                           id_network=id_network,
                           networks=res.json()['data']['networks'])


def remove_network(id_network):
    requests.get(api_remove_network.format(id_network), headers={'Token': request.cookies.get('token')})
    return redirect(url_for('admins_network'))


app.add_url_rule(url_admins_network, 'admins_network', admins_network, methods=['GET'])
app.add_url_rule(url_new_admin_network, 'create_admin', create_admin_network, methods=['POST'])
app.add_url_rule(url_ban_unban_network + '/<id_network>', 'ban_unban_network', ban_unban_network, methods=['GET'])
app.add_url_rule(url_update_admin_network + '/<id_network>', 'update_network', update_network, methods=['POST', 'GET'])
app.add_url_rule(url_remove_network + '/<id_network>', 'remove_network', remove_network, methods=['GET'])
