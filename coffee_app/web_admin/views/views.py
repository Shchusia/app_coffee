from coffee_app.web_admin.__init__ import app
from flask import render_template,\
    request,\
    redirect,\
    url_for,\
    make_response
from coffee_app.web_admin.urls.web.web_entry import url_index,\
    url_sign_in,\
    url_sign_out
from coffee_app.web_admin.urls.api.api_entry import api_check_valid_token,\
    api_sign_in,\
    api_sign_out
import requests
from coffee_app.config_app import role_admin_network, \
    role_admin_app
import hashlib
from coffee_app.web_admin.answer_web_server import status_code_not_200,\
    unknown_role
import coffee_app.web_admin.views.views_admin.views_admin_network
import coffee_app.web_admin.views.views_admin_network.view_shop
import coffee_app.web_admin.views.views_admin_network.view_category
import coffee_app.web_admin.views.views_admin_network.view_product
import coffee_app.web_admin.views.views_admin_network.view_news
import coffee_app.web_admin.views.views_admin_network.view_workers


def index():
    token = request.cookies.get('token')
    if token:
        res = requests.get(api_check_valid_token, headers={'Token': token})
        if res.status_code == 200:
            res = res.json()
            if res['message']['code'] > 0:  # значит валидны
                role = request.cookies.get('role')
                if role == role_admin_app:
                    # админ приложения
                    return redirect(url_for('admins_network'))
                elif role == role_admin_network:
                    # админ сети какой - то
                    return make_response(redirect(url_for('shops')))
                else:
                    return 'error'
    return redirect(url_for('sign_in'))


def sign_in():
    def create_hash_password(password):
        return hashlib.sha256(bytes(str(password), 'utf-8')).hexdigest()

    message = ''
    if request.method == 'POST':
        di = dict(request.form)
        data = {
            'login': di['login'][0],
            'password_hash': create_hash_password(di['password'][0])
        }
        res = requests.post(api_sign_in,
                            json=data)
        if res.status_code == 200:
            for_token = res
            res = res.json()
            if res['message']['code'] > 0:
                token = for_token.headers['Token']
                role = res['data']['role']
                id_admin = res['data']['id']
                if role == role_admin_app:
                    resp = make_response(redirect(url_for('admins_network')))
                elif role == role_admin_network:
                    resp = make_response(redirect(url_for('shops')))
                else:
                    return render_template('sign_in.html',
                                           message=unknown_role)
                resp.set_cookie('token', token)
                resp.set_cookie('id', str(id_admin))
                resp.set_cookie('role', role)
                return resp

            message += res['message']['body']
        else:
            message += status_code_not_200

    return render_template('sign_in.html',
                           message=message)


def sign_out():
    message = ''
    token = request.cookies.get('token')
    requests.get(api_sign_out, headers={'Token': token})
    resp = make_response(render_template('sign_in.html',
                                         message=message))
    resp.set_cookie('token', token)
    resp.set_cookie('id', str(0))
    resp.set_cookie('role', '')
    return resp


app.add_url_rule(url_index, 'index', index, methods=['POST', 'GET'])
app.add_url_rule(url_sign_in, 'sign_in', sign_in, methods=['POST', 'GET'])
app.add_url_rule(url_sign_out, 'logout', sign_out)
