from coffee_app.config_app import domain_web

url_workers = domain_web + '/workers'
url_worker_create = domain_web + '/worker/create'
url_worker_remove = domain_web + '/worker/remove'
url_worker_to_new_place = domain_web + '/worker/move'

url_worker_move_place = domain_web + '/worker/move'
