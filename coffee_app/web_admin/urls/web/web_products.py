from coffee_app.config_app import domain_web

url_list_products = domain_web + '/products'
url_create_product = domain_web + '/product/create'
url_remove_product = domain_web + '/product/remove'
url_product_copy = domain_web + '/products/copy'

url_new_version_product = domain_web + '/new_version_product'
url_global_products = domain_web + '/global_products'
