from coffee_app.config_app import domain_web

url_admins_network = domain_web + '/admins_network'
url_new_admin_network = domain_web + '/new_admin'
url_get_list_network = domain_web + '/list_admin'
url_ban_unban_network = domain_web + '/ban_unban'
url_update_admin_network = domain_web + '/network/update'
url_remove_network = domain_web + '/remove/network'
