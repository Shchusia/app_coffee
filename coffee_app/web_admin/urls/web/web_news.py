from coffee_app.config_app import domain_web

url_create_news = domain_web + '/news/create'
url_news = domain_web + '/news'
url_news_remove = domain_web + '/news/remove'
