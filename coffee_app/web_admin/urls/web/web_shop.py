from coffee_app.config_app import domain_web

url_shops = domain_web + '/shops'
url_create_shop = domain_web + '/create/shop'


url_categories = domain_web + '/categories'
url_products = domain_web + '/products'

url_update_gatherer = domain_web + '/gatherer/update'

url_ban_shop = domain_web + '/ban/shop'
url_remove_shop = domain_web + '/remove/shop'


