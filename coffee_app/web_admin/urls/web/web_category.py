from coffee_app.config_app import domain_web

url_categories = domain_web + '/categories'
url_create_category = domain_web + '/create_category'
url_update_category = domain_web + '/update_category'
url_remove_category = domain_web + '/remove_category'
