from coffee_app.config_app import domain_api,\
    base_url

api_upload_photo = base_url + domain_api + '/upload_file'
api_add_new_network = base_url + domain_api + '/create/network'
api_get_list_network = base_url + domain_api + '/networks'
api_ban_unban_network = base_url + domain_api + '/ban/unban/network'
api_update_network = base_url + domain_api + '/network/{}/update'
api_remove_network = base_url + domain_api + '/network/remove/{}'



api_share_gatherer_network = base_url + domain_api + '/gatherer/share/network'


