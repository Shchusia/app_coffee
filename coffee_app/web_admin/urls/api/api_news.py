from coffee_app.config_app import domain_api, \
    base_url
api_create_news = base_url + domain_api + '/news/create'
api_list_news = base_url + domain_api + '/news/list'
api_delete_news = base_url + domain_api + '/news/remove'
