from coffee_app.config_app import domain_api, \
    base_url


api_workers = base_url + domain_api + '/workers'
api_create_worker = base_url + domain_api + '/worker/create'
api_delete_worker = base_url + domain_api + '/worker/remove'
api_new_work_place = base_url + domain_api + '/worker/to/place'
