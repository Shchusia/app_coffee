from coffee_app.config_app import domain_api,\
    base_url

api_check_valid_token = base_url + domain_api + '/check_valid_token'
api_sign_in = base_url + domain_api + '/sign_in'
api_sign_out = base_url + domain_api + '/logout'
