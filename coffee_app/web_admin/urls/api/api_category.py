from coffee_app.config_app import domain_api, \
    base_url

api_create_category = base_url + domain_api + '/category/create'
api_get_categories = base_url + domain_api + '/categories'
api_update_categories = base_url + domain_api + '/category/update'
api_remove_category = base_url + domain_api + '/category/remove'
