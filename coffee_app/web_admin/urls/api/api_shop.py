from coffee_app.config_app import domain_api,\
    base_url

api_create_shops = base_url + domain_api + '/create/shop'
api_get_list_shops = base_url + domain_api + '/shops'
api_remove_shop = base_url + domain_api + '/remove/shop/{}'
api_ban_shop = base_url + domain_api + '/ban/shop/{}'
