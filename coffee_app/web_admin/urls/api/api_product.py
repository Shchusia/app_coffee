from coffee_app.config_app import domain_api,\
    base_url

api_create_product = base_url + domain_api + '/product/create'
api_get_list_products = base_url + domain_api + '/products'
api_toppings = base_url + domain_api + '/toppings'
api_product_remove = base_url + domain_api + '/product/remove'
api_copy_products = base_url + domain_api + '/products/copy'

api_new_product = base_url + domain_api + '/product/create/new'
api_get_consumable = base_url + domain_api + '/consumable'
api_get_list_products_new = base_url + domain_api + '/product/new/get/list'


