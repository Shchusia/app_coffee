from coffee_app.api_admin.__init__ import app,\
    check_valid_data_user
from coffee_app.config_app import MEDIA_FOLDER, \
    domain_photo,\
    str_path_to_file
import binascii
import os
from flask import request,\
    jsonify
import traceback
from coffee_app.api_admin.server_answers import good_upload_file,\
    arguments_error
from coffee_app.api_admin.other_functions.compress import comppress_photo
from coffee_app.api_admin.urls import api_upload_photo


MAX_FILE_SIZE = 1024 * 1024 + 1


@check_valid_data_user
def upload_file():
    try:

        file = request.files.get('file')
        if not os.path.exists(MEDIA_FOLDER + str_path_to_file):
            os.makedirs(MEDIA_FOLDER + str_path_to_file)
        name = (str(binascii.hexlify(os.urandom(20)))[2:42]) + '.{}'.format(file.filename.split('.')[-1])
        out = open(MEDIA_FOLDER + str_path_to_file + name, 'wb')
        while True:
            file_bytes = file.read(MAX_FILE_SIZE)
            out.write(file_bytes)
            if len(file_bytes) < MAX_FILE_SIZE:
                break
        out.close()
        comppress_photo(MEDIA_FOLDER + str_path_to_file + name)
        path = domain_photo + str_path_to_file + '.'.join(name.split('.')[:-1]) + '.jpeg'
        return jsonify({
                'code': 0,
                'message': good_upload_file,
                'data': {
                    'path_to_file': path
                }
        })
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


app.add_url_rule(api_upload_photo, 'upload_photo', upload_file, methods=['POST'])
