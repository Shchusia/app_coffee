from coffee_app.api_admin.__init__ import app,\
    check_valid_data_user
from flask import request,\
    jsonify
from coffee_app.api_admin.server_answers import arguments_error,\
    error_type_category
import traceback
from coffee_app.api_admin.entry.db_entry import token_to_data
from coffee_app.api_admin.category.db_category import db_create_category,\
    db_get_categories,\
    db_remove_category,\
    db_update_category

from coffee_app.api_admin.urls import api_create_category,\
    api_get_categories,\
    api_remove_category, \
    api_update_categories


@check_valid_data_user
def create_category():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        title_category = request.json['title_category']
        type_category = request.json['type_category']
        print(type_category)
        if type_category == 'drink' or type_category == 'other' or type_category == 'topping' or type_category == 'eat':
            if type_category == 'drink':
                type_category = 1
            elif type_category == 'topping':
                type_category = 2
            elif type_category == 'eat':
                type_category = 3
            else:
                type_category = 4
            return jsonify(db_create_category(data['id'],
                                              title_category,
                                              type_category))
        return jsonify({
            'code': 1,
            'message': error_type_category,
            'data': {}
        })

    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def get_categories():
    token = request.headers['Token']
    is_good, data = token_to_data(token)
    return jsonify(db_get_categories(data['id']))


@check_valid_data_user
def remove_category():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        id_category = request.headers['Id-Category']
        return jsonify(db_remove_category(data['id'],
                                          id_category))

    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def update_category():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        id_category = request.headers['Id-Category']
        title_category = request.json['title_category'],
        type_category = request.json['type_category']
        if type_category == 'drink' or type_category == 'other' or type_category == 'topping' or type_category == 'eat':
            if type_category == 'drink':
                type_category = 1
            elif type_category == 'other':
                type_category = 4
            elif type_category == 'eat':
                type_category = 3
            else:
                type_category = 2
            return jsonify(db_update_category(data['id'],
                                              id_category,
                                              title_category,
                                              type_category))
        return jsonify({
            'code': 1,
            'message': error_type_category,
            'data': {}
        })

    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


app.add_url_rule(api_create_category, 'create_category', create_category, methods=['POST'])
app.add_url_rule(api_get_categories, 'get_categories', get_categories, methods=['GET'])
app.add_url_rule(api_remove_category, 'remove_category', remove_category, methods=['GET'])
app.add_url_rule(api_update_categories, 'update_category', update_category, methods=['POST'])
