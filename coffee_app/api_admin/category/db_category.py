from coffee_app.api_admin.postgres import decorator_db
from coffee_app.api_admin.server_answers import good_select


@decorator_db
def db_create_category(id_network,
                       title_category,
                       type,
                       *args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        INSERT INTO category (id_category, title_category, ref_id_shop_coffee_network, ref_id_category_type) 
        VALUES (DEFAULT , '{}', {}, {} ) 
    '''.format(title_category.replace("'", "''"), id_network, type)
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }


@decorator_db
def db_get_categories(id_network,
                      *args,
                      **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_category, title_category, ref_id_category_type
        FROM category
        WHERE ref_id_shop_coffee_network = {}
    '''.format(id_network)
    cur.execute(select)
    res = [
        {
            'title_category': r[1],
            'id_category': r[0],
            'type_category': r[2]
        } for r in cur.fetchall()
    ]
    return {
        'code': 1,
        'message': good_select,
        'data': {
            'categories': res
        }
    }


@decorator_db
def db_remove_category(id_network,
                       id_category,
                       *args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = """
        DELETE FROM category WHERE ref_id_shop_coffee_network = {} AND id_category = {}
    """.format(id_network, id_category)
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }


@decorator_db
def db_update_category(id_network,
                       id_category,
                       title_category,
                       type,
                       *args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select_update = '''
        UPDATE category
        SET title_category = '{}',
        ref_id_category_type = {}
        WHERE id_category = {}
        AND ref_id_shop_coffee_network = {}
    '''.format(title_category, type, id_category, id_network)
    cur.execute(select_update)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }
