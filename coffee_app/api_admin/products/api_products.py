from coffee_app.api_admin.__init__ import app,\
    check_valid_data_user
from flask import request,\
    jsonify
from coffee_app.api_admin.server_answers import arguments_error,\
    error_coffee_shop,\
    error_unknown
from coffee_app.api_admin.entry.db_entry import token_to_data
from coffee_app.api_admin.products.db_products import db_create_product,\
    db_is_her_coffee_shop,\
    db_get_toppings,\
    db_get_products,\
    db_remove_product,\
    db_copy_product,\
    crate_main_product,\
    create_sub_product,\
    set_product_to_all_in_network,\
    db_get_other,\
    db_get_prod

from coffee_app.api_admin.urls import api_create_product, \
    api_get_list_products,\
    api_toppings,\
    api_product_remove,\
    api_copy_products,\
    api_new_product,\
    api_new_product,\
    api_get_consumable,\
    api_get_list_products_new

import json


@check_valid_data_user
def create_product():
    try:
        id_coffee_shop = request.json['id_coffee_shop']
        title_product = request.json['title_product']
        description_product = request.json['description_product']
        quantity = request.json['quantity']
        price = request.json['price']
        id_category = request.json['id_category']
        is_in_gatherer = request.json['is_in_gatherer']
        is_to_swap = request.json['is_to_swap']
        sub_product = request.json['sub_product']
        list_typing = request.json['list_typing']
        path_img = request.json['path_img']
        is_g, data = token_to_data(request.headers['Token'])
        if db_is_her_coffee_shop(id_coffee_shop, data['id']):
            return jsonify(db_create_product(id_coffee_shop,
                                             title_product,
                                             description_product,
                                             quantity,
                                             price,
                                             id_category,
                                             is_in_gatherer,
                                             is_to_swap,
                                             sub_product,
                                             list_typing,
                                             path_img))
        return jsonify({
            'code': 0,
            'message': error_coffee_shop,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def get_list_products():
    try:
        is_g, data = token_to_data(request.headers['Token'])
        id_shop = request.headers['Id-Coffee-Shop']
        if db_is_her_coffee_shop(id_shop, data['id']):
            return jsonify(db_get_products(id_shop,
                                           data['id']))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def get_list_products_new():
    try:
        is_g, data = token_to_data(request.headers['Token'])
        return jsonify(db_get_prod(data['id']))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def get_toppings():
    try:
        is_g, data = token_to_data(request.headers['Token'])
        return jsonify(db_get_toppings(data['id']))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def get_other():
    try:
        is_g, data = token_to_data(request.headers['Token'])
        return jsonify(db_get_other(data['id']))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def remove_product():
    try:
        is_g, data = token_to_data(request.headers['Token'])
        id_product = request.headers['Id-Product']
        return jsonify(db_remove_product(data['id'],
                                         id_product))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def copy_products():
    try:
        is_g, data = token_to_data(request.headers['Token'])
        id_coffee_shop_from = request.headers['From']
        id_coffee_shop_to = request.headers['To']

        if db_is_her_coffee_shop(id_coffee_shop_from,
                                 data['id']) \
                and db_is_her_coffee_shop(id_coffee_shop_to,
                                          data['id']):
            return jsonify(db_copy_product(id_coffee_shop_from, id_coffee_shop_to))

        return jsonify({
            'code': 0,
            'message': error_coffee_shop,
            'data': {}
        })

    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def new_product():
    try:
        is_g, data = token_to_data(request.headers['Token'])
        di = request.json
        id_product = crate_main_product(di['title'],
                                        di['path_img'],
                                        di['description'],
                                        di['options'],
                                        di['category'],
                                        di['toppings'],
                                        data['id'])
        print(id_product)
        if int(di['main_category']) == 1:
            for d in di['concrete_product']:
                select = '''
                    INSERT INTO concrete_product_info (id_concrete_product_info, sub_title, price, volume, dop_info, ref_id_product) 
                    VALUES  (DEFAULT, '{}', {}, {}, '{}', {} ) returning id_concrete_product_info;
                '''.format(d['sub_title'],
                           d['price'],
                           d['volume'],
                           json.dumps(d['consumable']),
                           id_product)
                print(select)
                id_sub_product = create_sub_product(select)
        elif int(di['main_category']) == 2:
            select = '''
                INSERT INTO concrete_product_info (id_concrete_product_info, price, volume, ref_id_product)
                VALUES (DEFAULT, {}, {}, {}) returning id_concrete_product_info
            '''.format(di['price'], di['volume'], id_product)
            id_sub_product = create_sub_product(select)
        elif int(di['main_category']) == 3:
            select = '''
              INSERT INTO concrete_product_info (id_concrete_product_info, price, ref_id_product)
              VALUES (DEFAULT, {},  {}) returning id_concrete_product_info
            '''.format(di['price'], id_product)
            id_sub_product = create_sub_product(select)
        else:
            select = '''
              INSERT INTO concrete_product_info (id_concrete_product_info, ref_id_product)
              VALUES (DEFAULT,  {}) returning id_concrete_product_info
            '''.format(id_product)
            id_sub_product = create_sub_product(select)

        if di['main_category'] == 4:
            set_product_to_all_in_network(data['id'], id_sub_product)
        return jsonify()

    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })
    except:
        return jsonify({
            'code': 0,
            'message': error_unknown,
            'data': {}
        })


app.add_url_rule(api_create_product, 'create_product', create_product, methods=['POST'])
app.add_url_rule(api_get_list_products, 'get_list_products', get_list_products, methods=['GET'])
app.add_url_rule(api_toppings, 'toppings', get_toppings, methods=['GET'])
app.add_url_rule(api_product_remove, 'remove_product', remove_product, methods=['GET'])
app.add_url_rule(api_copy_products, 'copy_products', copy_products, methods=['GET'])
app.add_url_rule(api_new_product, 'new_product', new_product, methods=['POST'])
app.add_url_rule(api_get_consumable, 'get_consumable', get_other, methods=['GET'])
app.add_url_rule(api_get_list_products_new, 'get_list_product', get_list_products_new, methods=['GET'])
