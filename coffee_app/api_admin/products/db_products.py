from coffee_app.api_admin.postgres import decorator_db
from coffee_app.api_admin.server_answers import good_select
import json
from decimal import Decimal


@decorator_db
def db_is_her_coffee_shop(id_coffee_shop,
                          id_admin,
                          *args,
                          **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_coffee_shop
        FROM coffee_shop
        WHERE id_coffee_shop = {}
        AND ref_id_coffee_shop_network = {}
    '''.format(id_coffee_shop, id_admin)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        if int(row[0]) == int(id_coffee_shop):
            return True
    return False


@decorator_db
def db_create_product(id_coffee_shop,
                      title_product,
                      description_product,
                      quantity,
                      price,
                      id_category,
                      is_in_gatherer,
                      is_to_swap,
                      sub_product,
                      list_typing,
                      path_to_img,
                      *args,
                      **kwargs):
    if list_typing:
        l_typing = 'ARRAY[{}]'.format(','.join(list_typing))
    else:
        l_typing = 'Null'
    select_create_product = '''
        INSERT INTO product ( id_product,
                              ref_id_category, 
                              ref_id_coffee_shop, 
                              title_product, 
                              description, 
                              quantity,
                              price, 
                              sub_product, 
                              list_typing, 
                              is_participates_in_gatherer, 
                              is_to_swap,
                              path_img) 
        VALUES (DEFAULT, {}, {}, '{}', '{}',{}, {}, '{}',{}, {}, {}, '{}' )
    '''.format(id_category,
               id_coffee_shop,
               title_product.replace("'", "''"),
               description_product.replace("'", "''"),
               quantity,
               Decimal(price),
               json.dumps(sub_product),
               l_typing,
               is_in_gatherer,
               is_to_swap,
               path_to_img)
    conn = kwargs['conn']
    cur = conn.cursor()
    cur.execute(select_create_product)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }


@decorator_db
def db_get_toppings(
                    *args,
                    **kwargs):

    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_product, main_title
        FROM category cat, product pr
        WHERE cat.ref_id_category_type = 2
        AND cat.ref_id_shop_coffee_network = {}
        AND cat.id_category = pr.ref_id_category;
    '''.format(args[0])
    cur.execute(select)
    res = [
        {
            'id_topping': r[0],
            'title_topping': r[1]
        } for r in cur.fetchall()
    ]
    return {
        'code': 1,
        'message': good_select,
        'data': {
            'toppings': res
        }
    }


@decorator_db
def db_get_other(*args,**kwargs):

    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_product, main_title
        FROM category cat, product pr
        WHERE cat.ref_id_category_type = 4
        AND cat.ref_id_shop_coffee_network = {}
        AND cat.id_category = pr.ref_id_category;
    '''.format(args[0])
    cur.execute(select)
    res = [
        {
            'id_other': r[0],
            'title_other': r[1]
        } for r in cur.fetchall()
    ]
    return {
        'code': 1,
        'message': good_select,
        'data': {
            'consumable': res
        }
    }


def db_get_concrete(cur,
                    id_product):
    select = '''
        SELECT id_concrete_product_info, sub_title, price, volume, dop_info
        FROM concrete_product_info
        WHERE  ref_id_product = {}
    '''.format(id_product)
    cur.execute(select)
    return [{
        'id_concrete': r[0],
        'sub_title': r[1],
        'price': str(r[2]) if r[2] else '',
        'volume': str(r[3]) if r[3] else '',
        'dop_info': r[4] if r[4] else ''

    } for r in cur.fetchall()]


@decorator_db
def db_get_prod(*args,
                **kwargs):
    select = '''
        SELECT c.id_category, c.title_category, c.ref_id_category_type
        FROM category c, coffee_shop_network csn
        WHERE c.ref_id_shop_coffee_network = csn.id_coffee_shop_network
        AND csn.id_coffee_shop_network = {}
    '''.format(args[0])
    conn = kwargs['conn']
    cur = conn.cursor()
    cur.execute(select)
    rows = cur.fetchall()
    data = {}
    for r in rows:
        select_get_product = '''
          SELECT p.id_product, p.main_title, p.path_to_img, p.description, p.other_option, p.topings
          FROM product p
          WHERE p.ref_id_category = {}
          ORDER BY p.id_product DESC
          '''.format(r[0])
        cur.execute(select_get_product)
        tmp_res = [{
            'id_product': r[0],
            'title': r[1],
            'path_img':r[2],
            'description': r[3],
            'other_option': r[4],
            'toppings': r[5],
            'concrete_products': db_get_concrete(cur, r[0])
        }for r in cur.fetchall()]
        data[r[0]] = {
            'title_category': r[1],
            'type_category': r[2],
            'products': tmp_res
        }
    return {
        'code': 1,
        'message': good_select,
        'data': {
            'products': data
        }
    }


@decorator_db
def db_get_products(id_shop,
                    id_admin,
                    *args,
                    **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT c.id_category, c.title_category, c.ref_id_category_type 
        FROM category c, coffee_shop_network csn 
        WHERE c.ref_id_shop_coffee_network = csn.id_coffee_shop_network
        AND csn.id_coffee_shop_network = {}
    '''.format(id_admin)
    cur.execute(select)
    rows = cur.fetchall()
    data = {}
    for r in rows:
        select_get_product = '''
            SELECT id_product,
                   ref_id_category, 
                   ref_id_coffee_shop, 
                   title_product, 
                   description, 
                   quantity,
                   price, 
                   sub_product, 
                   list_typing, 
                   is_participates_in_gatherer, 
                   is_to_swap,
                   path_img
            FROM product
            WHERE ref_id_coffee_shop = {}
            AND ref_id_category = {}
        '''.format(id_shop, r[0])
        cur.execute(select_get_product)
        temp = [
            {
                'id_product': t[0],
                'ref_id_category': t[1],
                'ref_id_coffee_shop': t[2],
                'title_product': t[3],
                'description': t[4],
                'quantity': t[5],
                'price': str(t[6]),
                'sub_product':t[7],
                'list_typing':t[8],
                'is_participates_in_gatherer':t[9],
                'is_to_swap':t[10],
                'path_img':t[11]
             } for t in cur.fetchall()
        ]
        data[r[0]] = {
            'title_category': r[1],
            'type_category': r[2],
            'products': temp
        }
    return {
        'code': 1,
        'message': good_select,
        'data': {
            'products': data
        }
    }


@decorator_db
def db_remove_product(id_admin,
                      id_product,
                      *args,
                      **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        DELETE FROM product WHERE id_product IN (
        SELECT p.id_product 
        FROM product p, coffee_shop cs, coffee_shop_network csn 
        WHERE p.id_product = {}
        AND p.ref_id_coffee_shop = cs.id_coffee_shop
        AND cs.ref_id_coffee_shop_network = csn.id_coffee_shop_network
        AND csn.id_coffee_shop_network = {}
        )
        AND id_product = {}
    '''.format(id_product, id_admin, id_product)
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }


@decorator_db
def db_copy_product(id_copy_from,
                    id_copy_to,
                    *args,
                    **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT p.ref_id_category, p.title_product, p.description, p.quantity, p.price, p.sub_product, p.list_typing, p.is_participates_in_gatherer, p.is_to_swap, p.path_img
        FROM product p
        WHERE p.ref_id_coffee_shop = {}
    '''.format(id_copy_from)
    cur.execute(select)
    rows = cur.fetchall()
    for r in rows:
        if r[6]:
            s = [str(i) for i in r[6]]
            l_typing = 'ARRAY[{}]'.format(','.join(s))
        else:
            l_typing = 'Null'
        select = '''
            INSERT INTO product (id_product, ref_id_category, ref_id_coffee_shop, title_product, description, quantity, price, sub_product, list_typing, is_participates_in_gatherer, is_to_swap, path_img) 
            VALUES (DEFAULT, {}, {}, '{}', '{}', {}, {}, '{}', {}, {}, {}, '{}') 
        '''.format(r[0], id_copy_to, r[1], r[2], r[3], r[4], json.dumps(r[5]), l_typing, r[7], r[8], r[9])
        cur.execute(select)
        conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }


@decorator_db
def is_exist_category_to_network(*args,
                                 **kwargs):
    id_network = args[0]
    id_category = args[1]
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_category
        FROM category
        WHERE ref_id_shop_coffee_network = {}
        AND id_category = {}
    '''.format(id_network, id_category)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        if int(row[0]) == int(id_category):
            return True

    return False


@decorator_db
def crate_main_product(*args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    title = args[0]
    path_img = args[1]
    description = args[2]
    other_option = args[3]
    ref_id_category = args[4]
    toppings = args[5]
    id_network = args[6]
    select = '''
        INSERT INTO product (id_product, main_title, path_to_img, description, other_option, ref_id_category, topings, ref_id_network) 
        VALUES (DEFAULT, '{}', '{}', '{}', '{}', {}, '{}',{} ) returning id_product
    '''.format(title,
               path_img,
               description,
               json.dumps(other_option),
               ref_id_category,
               '{'+','.join(toppings)+'}',
               id_network)
    print(select)
    cur.execute(select)
    row = cur.fetchone()
    conn.commit()
    return row[0]


@decorator_db
def create_sub_product(*args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = args[0]
    cur.execute(select)
    row = cur.fetchone()
    conn.commit()
    return row[0]


@decorator_db
def set_product_to_all_in_network(*args,
                                  **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    id_network = args[0]
    id_product = args[1]
    select = '''
        SELECT id_coffee_shop
        FROM coffee_shop
        WHERE ref_id_coffee_shop_network = {}
    '''.format(id_network)
    cur.execute(select)
    for i in cur.fetchall():
        select = '''
            INSERT INTO coffee_shop_to_product_accounting (ref_id_product, ref_id_coffee_shop) VALUES ({}, {})
        '''.format(id_product, i[0])
        cur.execute(select)
        conn.commit()


