from flask import request, \
    jsonify
from coffee_app.api_admin.__init__ import app
from coffee_app.api_admin.urls import api_sign_in,\
    api_check_valid_token,\
    api_sign_out
from coffee_app.api_admin.server_answers import arguments_error,\
    sign_in_error,\
    good_sign_in,\
    not_valid_token,\
    valid_token,\
    good_sign_out
from coffee_app.api_admin.entry.db_entry import db_sign_in,\
    token_to_data
from coffee_app.api_admin.redis.redb import set_client_multi,\
    check_valid_data_multi,\
    sign_out_multi


def sign_in():
    try:
        login = request.json['login']
        password = request.json['password_hash']
        data = db_sign_in(login, password)
        if data:
            answer = jsonify({
                'code': 1,
                'message': good_sign_in,
                'data': {
                    'role': data['role'],
                    'id': data['id']
                }
            })
            answer.headers.extend({
                'Token': data['token']
            })
            set_client_multi(data['id'],
                             data['token'],
                             data['role'])
            return answer

        return jsonify({
            'code': 1,
            'message': sign_in_error,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


def check_valid_token():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good:
            if check_valid_data_multi(data['id'],
                                      token,
                                      data['role']):
                return jsonify({
                    'code': 1,
                    'message': valid_token,
                    'data': {}
                })
            else:
                return jsonify({
                    'code': -1,
                    'message': not_valid_token,
                    'data': {}
                })
        return jsonify({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        })

    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


def sign_out():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good:
            if sign_out_multi(data['id'],
                              token,
                              data['role']):
                return jsonify({
                    'code': 1,
                    'message': good_sign_out,
                    'data': {}
                })
            else:
                return jsonify({
                    'code': -1,
                    'message': not_valid_token,
                    'data': {}
                })
        return jsonify({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


app.add_url_rule(api_sign_in, 'sign_in_api', sign_in, methods=['POST'])
app.add_url_rule(api_check_valid_token, 'check_valid_token', check_valid_token, methods=['GET'])
app.add_url_rule(api_sign_out, 'sign_out', sign_out, methods=['GET'])
