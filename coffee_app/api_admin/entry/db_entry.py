import jwt
from coffee_app.config_app import secret_word,\
    algorithm_encode,\
    role_admin_app,\
    role_admin_network
from coffee_app.api_admin.postgres import decorator_db
import traceback


def data_to_token(data):
    '''
    формирует токен по данным пользователя json
    :param data: данные пользователя имя пароль id
    :return: возвращает токен для пользователя
    '''

    return jwt.encode(data, secret_word, algorithm=algorithm_encode).decode()


def token_to_data(token):
    '''
    преобразовывает токен в данные пользователя
    :param token: токен который выдавался пользователю
    :return:json с данными пользователя
    '''
    try:
        return True, jwt.decode(token, secret_word, algorithms=[algorithm_encode])
    except jwt.exceptions.DecodeError:
        traceback.print_exc()
        return False, {}


@decorator_db
def db_sign_in(login, password, *args, **kwargs):
    conn = kwargs['conn']
    select_admin_find = '''
        SELECT id_admin, login, password_hash
        FROM admin
        WHERE login = '{}'
        AND password_hash = '{}'
    '''.format(login, password)

    select_admin_network_find = '''
        SELECT id_coffee_shop_network, login, password_hash
        FROM coffee_shop_network
        WHERE login = '{}'
        AND password_hash = '{}'
    '''.format(login, password)
    cur = conn.cursor()
    try:
        cur.execute(select_admin_find)
        row = cur.fetchone()
        if row:
            if row[1] == login and row[2] == password:
                data = {
                    'id': row[0],
                    'login': row[1],
                    'password': row[2],
                    'role': role_admin_app
                }
                token = data_to_token(data)
                data['token'] = token
                return data
            else:
                return None
    except:
        traceback.print_exc()
        conn.rollback()
        cur = conn.cursor()

    try:
        cur.execute(select_admin_network_find)
        row = cur.fetchone()
        if row:
            if row[1] == login and row[2] == password:
                data = {
                    'id': row[0],
                    'login': row[1],
                    'password': row[2],
                    'role': role_admin_network
                }
                token = data_to_token(data)
                data['token'] = token
                return data
            else:
                return None
    except:
        pass

    return None

