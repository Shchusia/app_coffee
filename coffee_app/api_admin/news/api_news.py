from coffee_app.api_admin.__init__ import app,\
    check_valid_data_user
from flask import request,\
    jsonify
from coffee_app.api_admin.server_answers import arguments_error
from coffee_app.api_admin.entry.db_entry import token_to_data
from coffee_app.api_admin.urls import api_create_news, \
    api_delete_news, \
    api_list_news
from coffee_app.api_admin.news.db_news import db_create_news, \
    db_get_list, \
    db_remove_news


@check_valid_data_user
def create_news():
    try:
        title_news = request.json['title_news']
        text_news = request.json['text_news']
        path_img = request.json['path_img']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        return jsonify(db_create_news(data['id'],
                                      title_news,
                                      text_news,
                                      path_img))

    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def list_news():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        return jsonify(db_get_list(data['id']))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def delete_news():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        id_news = request.headers['Id-News']
        return jsonify(db_remove_news(data['id'],
                                      id_news))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        })


app.add_url_rule(api_create_news, 'create_news', create_news, methods=['POST'])
app.add_url_rule(api_list_news, 'list_news', list_news, methods=['GET'])
app.add_url_rule(api_delete_news, 'delete_news', delete_news, methods=['GET'])
