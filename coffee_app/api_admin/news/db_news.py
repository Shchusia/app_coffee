from coffee_app.api_admin.postgres import decorator_db
from coffee_app.api_admin.server_answers import good_select


@decorator_db
def db_create_news(id_admin,
                   title_news,
                   text_news,
                   path_img,
                   *args,
                   **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        INSERT INTO news (id_news, ref_id_coffee_shop_network, title_news, text_news, path_img, created_on) 
        VALUES (DEFAULT, {}, '{}', '{}', '{}', DEFAULT) 
    '''.format(id_admin, title_news.replace("'", "''"), text_news.replace("'", "''"), path_img)
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }


@decorator_db
def db_get_list(id_admin,
                *args,
                **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_news, title_news, text_news, path_img, created_on
        FROM news
        WHERE ref_id_coffee_shop_network = {}
        ORDER BY id_news DESC 
    '''.format(id_admin)
    cur.execute(select)
    res = [
        {
            'id_news': r[0],
            'title_news': r[1],
            'text_news': r[2],
            'path_img': r[3],
            'date_create': '{}-{}-{} {}:{}'.format(r[4].day, r[4].month, r[4].year, r[4].hour, r[4].minute),
        } for r in cur.fetchall()
    ]
    return {
        'code': 1,
        'message': good_select,
        'data': {
            'news': res
        }
    }


@decorator_db
def db_remove_news(id_admin,
                   id_news,
                   *args,
                   **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        DELETE FROM news WHERE ref_id_coffee_shop_network = {} AND id_news = {}
    '''.format(id_admin, id_news)
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }
