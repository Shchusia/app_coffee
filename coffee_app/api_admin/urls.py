# domain_api = '/coffee/app/api/v1'
from coffee_app.config_app import domain_api

api_check_valid_token = domain_api + '/check_valid_token'
api_sign_in = domain_api + '/sign_in'
api_sign_out = domain_api + '/logout'

api_upload_photo = domain_api + '/upload_file'

api_add_new_network = domain_api + '/create/network'
api_get_list_network = domain_api + '/networks'
api_ban_unban_network = domain_api + '/ban/unban/network'
api_share_gatherer_network = domain_api + '/gatherer/share/network'
api_update_network = domain_api + '/network/<id_network>/update'
api_remove_network = domain_api + '/network/remove/<id_network>'


api_create_shops = domain_api + '/create/shop'

api_get_list_shops = domain_api + '/shops'
api_remove_shop = domain_api + '/remove/shop'
api_ban_shop = domain_api + '/ban/shop'


api_create_category = domain_api + '/category/create'
api_get_categories = domain_api + '/categories'
api_update_categories = domain_api + '/category/update'
api_remove_category = domain_api + '/category/remove'

api_create_product = domain_api + '/product/create'
api_get_list_products = domain_api + '/products'
api_toppings = domain_api + '/toppings'
api_product_remove = domain_api + '/product/remove'
api_copy_products = domain_api + '/products/copy'

api_create_news = domain_api + '/news/create'
api_list_news = domain_api + '/news/list'
api_delete_news = domain_api + '/news/remove'

api_workers = domain_api + '/workers'
api_create_worker = domain_api + '/worker/create'
api_delete_worker = domain_api + '/worker/remove'
api_new_work_place = domain_api + '/worker/to/place'

api_new_product = domain_api + '/product/create/new'
api_get_consumable = domain_api + '/consumable'
api_get_list_products_new = domain_api + '/product/new/get/list'
