from coffee_app.config_app import cache


def data_network_reset():
    cache.set('network', None)
