import traceback
from coffee_app.config_app import r

ttl_phone_in_redis = 300


def set_client_multi(id_user, session_key, role):
    r.lpush('{}_{}'.format(role, id_user), session_key)


def check_valid_data_multi(id_user, session_key, role):
    try:
        for li in list(r.lrange('{}_{}'.format(role,id_user), 0, -1)):
            if li.decode('utf-8') == session_key:
                return True
        return False
    except:
        traceback.print_exc()
    return False


def sign_out_multi(id_user, session_key, role):
    try:
        r.lrem('{}_{}'.format(role, id_user), 1, session_key)
    except:
        pass


def remove_user(id_user, role):
    try:
        for li in list(r.lrange('{}_{}'.format(role, id_user), 0, -1)):
            try:
                r.delete(li)
            except:
                traceback.print_exc()
                pass
        r.delete('{}_{}'.format(role, id_user))
    except:
        traceback.print_exc()
