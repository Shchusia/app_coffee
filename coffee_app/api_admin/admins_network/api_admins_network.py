from coffee_app.api_admin.__init__ import app,\
    check_valid_data_user
from coffee_app.api_admin.urls import api_add_new_network,\
    api_get_list_network,\
    api_ban_unban_network,\
    api_share_gatherer_network,\
    api_remove_network
from flask import request,\
    jsonify
from coffee_app.api_admin.admins_network.db_admins_network import db_add_new_network,\
    db_get_list_networks,\
    db_ban_un_ban_network,\
    db_share_gatherer,\
    db_remove_network
from coffee_app.api_admin.server_answers import arguments_error,\
    create_coffee_network,\
    error_in_select, \
    login_exist,\
    good_select
import traceback
from coffee_app.api_admin.entry.db_entry import token_to_data


@check_valid_data_user
def add_new_network():
    try:
        title = request.json['title']
        login = request.json['login']
        password = request.json['password']
        contacts = request.json['contacts']
        color = request.json['color']
        path_img = request.json['path_img']
        res = db_add_new_network(title,
                                 login,
                                 password,
                                 contacts,
                                 color,
                                 path_img)
        if res > 0:
            return jsonify({
                'code': 1,
                'message': create_coffee_network,
                'data': {}
            })
        elif res == -1:
            return jsonify({
                'code': 1,
                'message': login_exist,
                'data': {}
            })
        else:
            return jsonify({
                'code': 1,
                'message': error_in_select,
                'data': {}
            })
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def get_list_networks():
    return jsonify({
        'code': 1,
        'message': good_select,
        'data': {
            'networks': db_get_list_networks()
        }
    })


@check_valid_data_user
def ban_unban_network():
    try:
        id_network = request.headers['id_network']
        db_ban_un_ban_network(id_network)
        return jsonify()
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def share_gatherer():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        is_gatherer_shares = request.json['is_gatherer_shares']
        quantity_gatherer = request.json['quantity_gatherer']
        return jsonify(db_share_gatherer(data['id'],
                                         is_gatherer_shares,
                                         quantity_gatherer))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def update_admin(id_admin):
    try:
        title = request.json['title']
        login = request.json['login']
        password = request.json['password']
        contacts = request.json['contacts']
        color = request.json['color']
        path_img = request.json['path_img']
        res = db_add_new_network(title,
                                 login,
                                 password,
                                 contacts,
                                 color,
                                 path_img)
        if res > 0:
            return jsonify({
                'code': 1,
                'message': create_coffee_network,
                'data': {}
            })
        elif res == -1:
            return jsonify({
                'code': 1,
                'message': login_exist,
                'data': {}
            })
        else:
            return jsonify({
                'code': 1,
                'message': error_in_select,
                'data': {}
            })
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def remove_network(id_network):
    return jsonify(db_remove_network(id_network))


app.add_url_rule(api_add_new_network, 'add_new_network', add_new_network, methods=['POST'])
app.add_url_rule(api_get_list_network, 'list_network', get_list_networks, methods=['GET'])
app.add_url_rule(api_ban_unban_network, 'ban_unban', ban_unban_network, methods=['GET'])
app.add_url_rule(api_share_gatherer_network, 'share_gatherer', share_gatherer, methods=['POST'])
app.add_url_rule(api_remove_network, 'remove_network', remove_network, methods=['GET'])
