from coffee_app.api_admin.postgres import decorator_db
from coffee_app.api_admin.server_answers import good_select
from coffee_app.api_admin.memcache.memdb import data_network_reset

@decorator_db
def db_add_new_network(title,
                       login,
                       password,
                       contacts,
                       color,
                       path_img,
                       *args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select_find_admin_with_new_login = '''
        SELECT id_admin
        FROM admin
        WHERE login = '{}'
    '''.format(login)
    select_find_admin_network_with_new_login = '''
        SELECT id_coffee_shop_network
        FROM coffee_shop_network
        WHERE login = '{}'
    '''.format(login)

    insert_new_network = '''
        INSERT INTO coffee_shop_network ( id_coffee_shop_network, 
                                          title_network, 
                                          date_create,
                                          is_activate, 
                                          is_ban, 
                                          color_hash,
                                          login, 
                                          password_hash, 
                                          contacts, 
                                          path_img)
         VALUES (DEFAULT, '{}',DEFAULT,FALSE ,FALSE ,'{}', '{}', '{}', '{}','{}')
    '''.format(title.replace("'", "''"),
               color,
               login,
               password,
               contacts.replace("'", "''"),
               path_img)
    cur.execute(select_find_admin_with_new_login)
    row = cur.fetchone()
    if row:
        return -1
    cur.execute(select_find_admin_network_with_new_login)
    row = cur.fetchone()
    if row:
        return -1
    try:
        cur.execute(insert_new_network)
        conn.commit()
        return 1
    except:
        return -2


@decorator_db
def db_get_list_networks(*args,
                         **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select_get_networks = '''
        SELECT id_coffee_shop_network, title_network, contacts, is_activate, is_ban, color_hash, path_img,login
        FROM coffee_shop_network
        ORDER BY id_coffee_shop_network DESC, is_ban DESC 
    '''
    cur.execute(select_get_networks)
    res = [{
        'id_network': r[0],
        'title_network': r[1],
        'contacts': r[2],
        'is_activate': r[3],
        'is_ban': r[4],
        'color_hash': r[5],
        'path_img':r[6],
        'login':r[7]
    }for r in cur.fetchall()]
    return res


@decorator_db
def db_ban_un_ban_network(id_network,
                       *args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select_update_status_network = '''
        UPDATE coffee_shop_network
        SET is_ban = NOT is_ban
        WHERE id_coffee_shop_network = {}
    '''.format(id_network)
    cur.execute(select_update_status_network)
    conn.commit()
    data_network_reset()


@decorator_db
def db_share_gatherer(id_admin,
                      is_gatherer_shares,
                      quantity_gatherer,
                      *args,
                      **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select_update_gatherer = '''
        UPDATE coffee_shop_network
        SET is_gatherer_shares = {},
        quantity_gatherer = {}
        WHERE id_coffee_shop_network = {}
    '''.format(is_gatherer_shares,
               quantity_gatherer,
               id_admin)
    cur.execute(select_update_gatherer)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }


@decorator_db
def db_remove_network(id_network,
                      *args,
                      **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select_remove_network = '''
        DELETE FROM coffee_shop_network WHERE id_coffee_shop_network = {}
    '''.format(id_network)
    cur.execute(select_remove_network)
    conn.commit()
    data_network_reset()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }

