from coffee_app.api_admin.__init__ import app,\
    check_valid_data_user
from flask import request,\
    jsonify
from coffee_app.api_admin.server_answers import arguments_error,\
    error_coffee_shop
from coffee_app.api_admin.entry.db_entry import token_to_data
from coffee_app.api_admin.urls import api_create_worker, \
    api_delete_worker, \
    api_workers,\
    api_new_work_place
from coffee_app.api_admin.workers.db_worker import db_get_list_worker,\
    db_new_worker,\
    db_remove_worker,\
    db_to_new_place
from coffee_app.api_admin.products.db_products import db_is_her_coffee_shop


@check_valid_data_user
def create_worker():
    try:
        full_name = request.json['full_name']
        phone = request.json['phone']
        login = request.json['login']
        password = request.json['password']
        id_coffee_shop = request.json['id_coffee_shop']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if db_is_her_coffee_shop(id_coffee_shop, data['id']):
            return jsonify(db_new_worker(data['id'],
                                         full_name,
                                         phone,
                                         login,
                                         password,
                                         id_coffee_shop))
        return jsonify({
            'code': 1,
            'message': error_coffee_shop,
            'data': {}
        })

    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def get_list_worker():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        return jsonify(db_get_list_worker(data['id']))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def delete_worker():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        id_worker = request.headers['Id-Worker']
        return jsonify(db_remove_worker(data['id'],
                                        id_worker))
    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def to_new_place():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        id_worker = request.headers['Id-Worker']
        id_shop = request.headers['Id-Coffee-Shop']
        if db_is_her_coffee_shop(id_shop, data['id']):
            return jsonify(db_to_new_place(data['id'],
                                           id_worker,
                                           id_shop))
        return jsonify({
            'code': 1,
            'message': error_coffee_shop,
            'data': {}
        })

    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        })


app.add_url_rule(api_create_worker, 'create_worker', create_worker, methods=['POST'])
app.add_url_rule(api_workers, 'workers', get_list_worker, methods=['GET'])
app.add_url_rule(api_delete_worker, 'delete_worker', delete_worker, methods=['GET'])
app.add_url_rule(api_new_work_place, 'to_new_place', to_new_place, methods=['GET'])
