from coffee_app.api_admin.postgres import decorator_db
from coffee_app.api_admin.server_answers import good_select,\
    login_exist
import traceback

@decorator_db
def db_new_worker(id_admin,
                  full_name,
                  phone,
                  login,
                  password,
                  id_coffee_shop,
                  *args,
                  **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    try:
        select = '''
            INSERT INTO worker (id_worker, full_name, phone, login, password_hash, ref_id_coffee_shop_network, ref_id_coffee_shop) 
            VALUES (DEFAULT, '{}', '{}', '{}', '{}', {},{})
        '''.format(full_name,
                   phone,
                   login,
                   password,
                   id_admin,
                   id_coffee_shop)
        # print(select)
        cur.execute(select)
        conn.commit()
        return {
            'code': 1,
            'message': good_select,
            'data': {}
        }
    except:
        traceback.print_exc()
        return {
            'code': 1,
            'message': login_exist,
            'data': {}
        }


@decorator_db
def db_get_list_worker(id_admin,
                       *args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_worker, full_name, login, phone, ref_id_coffee_shop
        FROM worker
        WHERE ref_id_coffee_shop_network = {}
    '''.format(id_admin)
    cur.execute(select)
    rows = [
        {
            'id_worker': r[0],
            'full_name': r[1],
            'login': r[2],
            'phone': r[3],
            'id_coffee_shop': r[4]
        }for r in cur.fetchall()
    ]

    select = '''
        SELECT id_coffee_shop, title
        FROM coffee_shop
        WHERE ref_id_coffee_shop_network = {}
    '''.format(id_admin)
    cur.execute(select)
    shops = {
        r[0]: r[1]
        for r in cur.fetchall()
    }

    return {
        'code': 1,
        'message': good_select,
        'data': {
            'workers': rows,
            'shops': shops
        }
    }


@decorator_db
def db_remove_worker(id_admin,
                     id_worker,
                     *args,
                     **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        DELETE FROM worker WHERE id_worker = {} AND  ref_id_coffee_shop_network = {}
    '''.format(id_worker,
               id_admin)
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }


@decorator_db
def db_to_new_place(id_admin,
                    id_worker,
                    id_shop,
                    *args,
                    **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        UPDATE worker
        SET ref_id_coffee_shop = {}
        WHERE id_worker = {}
        AND ref_id_coffee_shop_network = {}
    '''.format(id_shop,
               id_worker,
               id_admin)
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }
