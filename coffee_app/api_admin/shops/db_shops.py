from coffee_app.api_admin.postgres import decorator_db
import requests
from coffee_app.api_admin.server_answers import good_select


@decorator_db
def db_get_city_by_coordinates(lat,
                               lng,
                               *args,
                               **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    res = requests.get('https://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&key=AIzaSyAtdcwr5Dojv-zdFs3IMs1Cl2vAFkpoj08&language=ru'.format(lat,
                                                                                                                                                      lng)).json()
    city = None
    res = res['results']
    try:
        for i in res[2]['address_components']:
            try:

                index = i['types'].index('locality')
                city = i['long_name']
            except ValueError:
                pass
        if city is None:
            for i in res[2]['address_components']:
                try:

                    index = i['types'].index('administrative_area_level_1')
                    city = (i['long_name'])
                except ValueError:
                    pass
    except:
        city = 'ХЗ'
    if city is None:
        city = 'ХЗ'

    select_get_city = '''
        SELECT id_city, title_city
        FROM city
        WHERE title_city = '{}'
    '''.format(city)
    cur.execute(select_get_city)
    row = cur.fetchone()
    if row:
        return row[0]
    else:
        select_insert_city = '''
            INSERT INTO city (id_city, title_city) VALUES (DEFAULT, '{}') RETURNING id_city
        '''.format(city.replace("'", "''"))
        cur.execute(select_insert_city)
        row = cur.fetchone()
        conn.commit()
        return row[0]


@decorator_db
def db_create_shop(path_img,
                   title,
                   description,
                   time_start,
                   time_end,
                   latitude,
                   longitude,
                   address,
                   week,
                   id_city,
                   id_network,
                   *args,
                   **kwargs):
    conn = kwargs['conn']
    select_insert_shop = '''
        INSERT INTO coffee_shop ( id_coffee_shop, 
                                  ref_id_coffee_shop_network, 
                                  title, 
                                  description, 
                                  location, 
                                  working_day, 
                                  time_start, 
                                  time_end, 
                                  path_img, 
                                  ref_id_city, 
                                  is_activate, 
                                  address) 
        VALUES (DEFAULT, {}, '{}', '{}', 'SRID=4326;POINT({} {})', ARRAY[{}],'{}', '{}', '{}',{}, TRUE, '{}')
    '''.format(id_network,
               title.replace("'", "''"),
               description.replace("'", "''"),
               latitude,
               longitude,
               week,
               time_start,
               time_end,
               path_img,
               id_city,
               address.replace("'", "''"))
    cur = conn.cursor()
    cur.execute(select_insert_shop)
    conn.commit()
    return {
        'code': 1,
        'message': good_select,
        'data': {}
    }


# ST_x(ST_Transform( ea.location, 4326)), ST_y(ST_Transform( ea.location, 4326))


@decorator_db
def db_get_shops(id_network,
                 *args,
                 **kwargs):
    conn = kwargs['conn']
    select = '''
        SELECT id_coffee_shop, title, working_day, time_start, time_end, address, is_activate, path_img, description
        FROM coffee_shop
        WHERE ref_id_coffee_shop_network = {}
        ORDER BY  is_activate DESC  ,id_coffee_shop DESC
    '''.format(id_network)
    cur = conn.cursor()
    select_get_gatherer_data = '''
        SELECT is_gatherer_shares, quantity_gatherer
        FROM coffee_shop_network
        WHERE id_coffee_shop_network = {}
    '''.format(id_network)
    cur.execute(select_get_gatherer_data)
    row = cur.fetchone()
    cur.execute(select)
    res = [
        {
            'id_shop': r[0],
            'title': r[1],
            'working_day': r[2],
            'time_start': str(r[3]),
            'time_end': str(r[4]),
            'address': r[5],
            'is_activate': r[6],
            'path_img': r[7],
            'description': r[8]
        }
        for r in cur.fetchall()
    ]
    return {
        'code': 1,
        'message': good_select,
        'data': {
            'shops': res,
            'gatherer': {
                'is_gatherer_shares': row[0],
                'quantity_gatherer': row[1]
            }
        }
    }


@decorator_db
def db_ban_un_ban_shop(id_shop,
                       id_admin,
                       *args,
                       **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select_update_status_network = '''
        UPDATE coffee_shop
        SET is_activate = NOT is_activate
        WHERE id_coffee_shop = {}
        AND ref_id_coffee_shop_network = {}
    '''.format(id_shop,
               id_admin)
    cur.execute(select_update_status_network)
    conn.commit()


@decorator_db
def db_remove_shop(id_shop,
                   id_admin,
                   *args,
                   **kwargs):
        conn = kwargs['conn']
        cur = conn.cursor()
        select_remove_network = '''
            DELETE FROM coffee_shop  
            WHERE id_coffee_shop = {}
                    AND ref_id_coffee_shop_network = {}
        '''.format(id_shop,
                   id_admin)
        cur.execute(select_remove_network)
        conn.commit()
        return {
            'code': 1,
            'message': good_select,
            'data': {}
        }
