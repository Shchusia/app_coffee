from coffee_app.api_admin.__init__ import app,\
    check_valid_data_user
from coffee_app.api_admin.urls import api_create_shops,\
    api_get_list_shops,\
    api_remove_shop,\
    api_ban_shop
from flask import request,\
    jsonify
from coffee_app.api_admin.shops.db_shops import db_create_shop,\
    db_get_city_by_coordinates,\
    db_get_shops,\
    db_ban_un_ban_shop,\
    db_remove_shop
from coffee_app.api_admin.server_answers import arguments_error
import traceback
from coffee_app.api_admin.entry.db_entry import token_to_data


@check_valid_data_user
def create_shop():
    try:
        path_img = request.json['path_img']
        title = request.json['title']
        description = request.json['description']
        time_start = request.json['time_start']
        time_end = request.json['time_end']
        latitude = request.json['latitude']
        longitude = request.json['longitude']
        address = request.json['address']
        week = ','.join(request.json['week'])
        id_city = db_get_city_by_coordinates(latitude,
                                             longitude)
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        return jsonify(db_create_shop(path_img,
                                      title,
                                      description,
                                      time_start,
                                      time_end,
                                      latitude,
                                      longitude,
                                      address,
                                      week,
                                      id_city,
                                      data['id']))

    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


@check_valid_data_user
def get_list_shops():
    token = request.headers['Token']
    is_good, data = token_to_data(token)
    res = db_get_shops(data['id'])
    # print(res)
    return jsonify(res)


@check_valid_data_user
def ban_shop(id_shop):
    token = request.headers['Token']
    is_good, data = token_to_data(token)
    return jsonify(db_ban_un_ban_shop(id_shop,
                                      data['id']))


@check_valid_data_user
def remove_shop(id_shop):
    token = request.headers['Token']
    is_good, data = token_to_data(token)
    return jsonify(db_remove_shop(id_shop,
                                  data['id']))


app.add_url_rule(api_create_shops, 'create_shops', create_shop, methods=['POST'])
app.add_url_rule(api_get_list_shops, 'get_list_shops', get_list_shops, methods=['GET'])
app.add_url_rule(api_ban_shop + '/<id_shop>', 'ban_shop', ban_shop, methods=['GET'])
app.add_url_rule(api_remove_shop + '/<id_shop>', 'remove_shop', remove_shop, methods=['GET'])
