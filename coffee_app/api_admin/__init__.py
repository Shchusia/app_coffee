from flask import Flask,render_template,request,redirect,url_for,session,jsonify,blueprints
import traceback
import logging
from logging.handlers import RotatingFileHandler
from celery import Celery
from coffee_app.config_app import path_to_celery
from coffee_app.api_admin.entry.db_entry import token_to_data
from coffee_app.api_admin.redis.redb import check_valid_data_multi
from coffee_app.api_admin.server_answers import not_valid_token,\
    arguments_error
app = Flask(__name__)
handler = RotatingFileHandler('foo.log', maxBytes=10000, backupCount=1)
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)
async_mode = None


app.config['CELERY_BROKER_URL'] = path_to_celery
app.config['CELERY_RESULT_BACKEND'] = path_to_celery
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


def check_valid_data_user(fn):
    def wrap(*args, **kwargs):
        try:
            token = request.headers['Token']
            is_good, data = token_to_data(token)
            if is_good:
                if check_valid_data_multi(data['id'],
                                          token,
                                          data['role']):
                    return fn(*args,
                              **kwargs)
                else:
                    return jsonify({
                        'code': -1,
                        'message': not_valid_token,
                        'data': {}
                    })
            return jsonify({
                'code': -1,
                'message': not_valid_token,
                'data': {}
            })

        except KeyError:
            return jsonify({
                'code': 0,
                'message': arguments_error,
                'data': {}
            })
    return wrap


from coffee_app.api_admin.entry import api_entry
from coffee_app.api_admin.files import api_file
from coffee_app.api_admin.admins_network import api_admins_network
from coffee_app.api_admin.shops import api_shops
from coffee_app.api_admin.category import api_category
from coffee_app.api_admin.products import api_products
from coffee_app.api_admin.news import api_news
from coffee_app.api_admin.workers import api_worker
