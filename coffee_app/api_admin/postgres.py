import psycopg2
from coffee_app.config_app import str_connect_to_db
import traceback
from coffee_app.api_admin.server_answers import error_in_select,\
    error_in_connect


def create_connection():
    """
    метод создания подключения к БД
    :return: или подключение или None
    """
    conn = None
    try:
        conn = psycopg2.connect(str_connect_to_db)
    except:
        print("I am unable to connect to the database")
    return conn


def decorator_db(fn):
    def wrap(*args, **kwargs):
        conn = create_connection()
        try:
            if conn:
                kwargs['conn'] = conn
                data = fn(*args,
                          conn,
                          **kwargs)
                conn.close()
                return data
            return {
                'code': 0,
                'message': error_in_connect,
                'data': {}
            }
        except:
            traceback.print_exc()
            conn.close()
            return {
                'code': 0,
                'message': error_in_select,
                'data': {}
            }
    return wrap


