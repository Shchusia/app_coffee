CREATE SEQUENCE concrete_product_info_id START 1;

CREATE EXTENSION postgis;
CREATE EXTENSION btree_gist;

CREATE SEQUENCE comments_write_off_id START 1;

CREATE SEQUENCE news_id START 1;
CREATE SEQUENCE worker_id START 1;

CREATE SEQUENCE admin_id START 1;
CREATE SEQUENCE coffee_shop_network_id START 1;
CREATE SEQUENCE coffee_shop_id START 1;
CREATE SEQUENCE city_id START 1;

CREATE SEQUENCE category_type_id START 1;
CREATE SEQUENCE category_id START 1;
CREATE SEQUENCE product_id START 1;

CREATE SEQUENCE user_id START 1;
CREATE SEQUENCE user_push_data_id START 1;
CREATE SEQUENCE user_to_social_id START 1;
CREATE SEQUENCE social_id START 1;

CREATE SEQUENCE order_id START 1;
CREATE SEQUENCE order_product_id START 1;
CREATE SEQUENCE order_status_id START 1;

CREATE TABLE admin (
  id_admin      INT PRIMARY KEY             DEFAULT nextval('admin_id'),
  login         VARCHAR(100) UNIQUE,
  password_hash VARCHAR(100),
  created_on    TIMESTAMP WITHOUT TIME ZONE DEFAULT timezone('utc' :: TEXT, now()),
  contacts      VARCHAR(500)
);

CREATE TABLE coffee_shop_network (
  id_coffee_shop_network INT PRIMARY KEY             DEFAULT nextval('coffee_shop_network_id'),
  title_network          VARCHAR(100) NOT NULL,
  date_create            TIMESTAMP WITHOUT TIME ZONE DEFAULT timezone('utc' :: TEXT, now()),
  date_activate          TIMESTAMP WITHOUT TIME ZONE DEFAULT timezone('utc' :: TEXT, now()),
  is_activate            BOOLEAN                     DEFAULT FALSE,
  is_ban                 BOOLEAN                     DEFAULT FALSE,
  color_hash             VARCHAR(100) NOT NULL,
  login                  VARCHAR(100) NOT NULL UNIQUE,
  password_hash          VARCHAR(100) NOT NULL,
  contacts               TEXT,
  is_gatherer_shares     BOOLEAN                     DEFAULT FALSE,
  quantity_gatherer      INT                         DEFAULT 0,
  path_img               VARCHAR(200)
);

CREATE TABLE city (
  id_city    INT PRIMARY KEY DEFAULT nextval('city_id'),
  title_city VARCHAR(100)
);

CREATE TABLE coffee_shop (
  id_coffee_shop             INT PRIMARY KEY DEFAULT nextval('coffee_shop_id'),
  ref_id_coffee_shop_network INT REFERENCES coffee_shop_network (id_coffee_shop_network) ON DELETE CASCADE,
  title                      VARCHAR(100) NOT NULL,
  description                VARCHAR(500),
  location                   GEOMETRY(POINT, 4326),
  working_day                BOOLEAN ARRAY [7],
  time_start                 TIME,
  time_end                   TIME,
  path_img                   VARCHAR(500),
  ref_id_city                INT REFERENCES city (id_city),
  address                    VARCHAR(500),
  is_activate                BOOLEAN         DEFAULT TRUE,
  code_for_entry             VARCHAR(10)     DEFAULT '000000'
);

CREATE TABLE category_type (
  id_category_type INT PRIMARY KEY DEFAULT nextval('category_type_id'),
  type_category    VARCHAR(100)
);

CREATE TABLE category (
  id_category                INT PRIMARY KEY DEFAULT nextval('category_id'),
  title_category             VARCHAR(100),
  ref_id_shop_coffee_network INT REFERENCES coffee_shop_network (id_coffee_shop_network) ON DELETE CASCADE,
  ref_id_category_type       INT REFERENCES category_type (id_category_type)
);

CREATE TABLE product (
  id_product      INT PRIMARY KEY DEFAULT nextval('product_id'),
  main_title      VARCHAR(100),
  path_to_img     VARCHAR(200),
  description     VARCHAR(1000),
  other_option    JSON,
  ref_id_category INT REFERENCES category (id_category) ON DELETE CASCADE,
  topings         INT ARRAY,
  ref_id_network  INT REFERENCES coffee_shop_network (id_coffee_shop_network)
);


CREATE TABLE concrete_product_info (
  id_concrete_product_info INT PRIMARY KEY DEFAULT nextval('concrete_product_info_id'),
  sub_title                VARCHAR(100)    DEFAULT '',
  price                    DECIMAL,
  volume                   DECIMAL,
  dop_info                 JSON,
  ref_id_product           INT REFERENCES product (id_product) ON DELETE CASCADE

);

CREATE TABLE coffee_shop_to_product_menu (
  -- каждый бариста выбирает для точки
  ref_id_product     INT REFERENCES concrete_product_info (id_concrete_product_info) ON DELETE CASCADE,
  ref_id_coffee_shop INT REFERENCES coffee_shop (id_coffee_shop) ON DELETE CASCADE
);

CREATE TABLE coffee_shop_to_product_accounting (
  -- когда добавляется новый расходный материал закидывать сразу всем
  ref_id_product     INT REFERENCES concrete_product_info (id_concrete_product_info) ON DELETE CASCADE,
  ref_id_coffee_shop INT REFERENCES coffee_shop (id_coffee_shop) ON DELETE CASCADE,
  quantity           INT DEFAULT 0,
  PRIMARY KEY (ref_id_coffee_shop, ref_id_product)

);


CREATE TABLE worker (
  id_worker                  INT PRIMARY KEY DEFAULT nextval('worker_id'),
  full_name                  VARCHAR(100),
  phone                      VARCHAR(20),
  login                      VARCHAR(100) UNIQUE,
  password_hash              VARCHAR(150),
  ref_id_coffee_shop_network INT REFERENCES coffee_shop_network (id_coffee_shop_network) ON DELETE CASCADE,
  ref_id_coffee_shop         INT
);

CREATE TABLE users (
  id_user INT PRIMARY KEY DEFAULT nextval('user_id')
);

CREATE TABLE order_status (
  id_order_status INT PRIMARY KEY DEFAULT nextval('order_status_id'),
  order_status    VARCHAR(100)
);


CREATE TABLE orders (
  id_order       INT PRIMARY KEY             DEFAULT nextval('order_id'),
  rew_id_worker  INT,
  ref_id_user    INT,
  ref_id_shop    INT,
  ref_id_network INT REFERENCES coffee_shop_network (id_coffee_shop_network) ON DELETE CASCADE,
  ref_id_status  INT REFERENCES order_status (id_order_status),
  created_on     TIMESTAMP WITHOUT TIME ZONE DEFAULT timezone('utc' :: TEXT, now()),
  price          DECIMAL
);

CREATE TABLE order_product (
  id_order_product INT PRIMARY KEY DEFAULT nextval('order_product_id'),
  ref_id_order     INT REFERENCES orders (id_order),
  ref_id_product   INT,
  quantity         INT,
  toppings         INT ARRAY,
  orher_inf        JSON,
  price            DECIMAL
);





/*
CREATE TABLE product(
  id_product INT PRIMARY KEY DEFAULT nextval('product_id'),
  ref_id_category INT REFERENCES category(id_category) ON DELETE CASCADE ,
  ref_id_coffee_shop INT REFERENCES coffee_shop(id_coffee_shop) ON DELETE CASCADE ,
  title_product VARCHAR(100),
  description VARCHAR(100),
  quantity INT,
  price DECIMAL,
  sub_product JSON,
  list_typing INT[],
  is_participates_in_gatherer BOOLEAN DEFAULT FALSE,
  is_to_swap BOOLEAN DEFAULT FALSE,
  path_img VARCHAR(100)
);

CREATE TABLE news(
  id_news INT PRIMARY KEY DEFAULT nextval('news_id'),
  ref_id_coffee_shop_network INT REFERENCES coffee_shop_network(id_coffee_shop_network) on DELETE CASCADE ,
  title_news VARCHAR(100),
  text_news VARCHAR(1000),
  path_img VARCHAR(200),
  created_on timestamp without time zone DEFAULT timezone('utc'::text, now())
);

CREATE TABLE worker(
  id_worker INT PRIMARY KEY DEFAULT nextval('worker_id'),
  full_name VARCHAR(100),
  phone VARCHAR(20),
  login VARCHAR(100) UNIQUE,
  password_hash VARCHAR(150),
  ref_id_coffee_shop_network INT REFERENCES coffee_shop_network(id_coffee_shop_network) ON DELETE CASCADE,
  ref_id_coffee_shop INT
);

CREATE TABLE social(
  id_social INT PRIMARY KEY DEFAULT nextval('social_id'),
  title_social VARCHAR(100)
);

CREATE TABLE users(
  id_user INT PRIMARY KEY DEFAULT nextval('user_id'),
  full_name VARCHAR(100),
  path_img VARCHAR(200),
  phone VARCHAR(20),
  mail VARCHAR(100),
  password_hash VARCHAR(200),
  date_create timestamp without time zone DEFAULT timezone('utc'::text, now()),
  is_ban BOOLEAN DEFAULT FALSE,
  is_social BOOLEAN DEFAULT FALSE
);

CREATE TABLE user_to_social(
  id_user_to_social INT PRIMARY KEY DEFAULT nextval('user_to_social_id'),
  ref_id_social INT REFERENCES social(id_social),
  ref_id_user INT REFERENCES users(id_user),
  token VARCHAR(300),
  id_in_social VARCHAR(100)
);

CREATE TABLE user_push_data(
  id_user_push_data INT PRIMARY KEY DEFAULT nextval('user_push_data_id'),
  ref_id_user INT REFERENCES users(id_user),
  is_ios BOOLEAN,
  push_key VARCHAR(500)
);

CREATE TABLE order_status(
  id_order_status INT PRIMARY KEY DEFAULT nextval('order_status_id'),
  status VARCHAR(100)
);

CREATE TABLE orders(
  id_order INT PRIMARY KEY DEFAULT nextval('order_id'),
  code_order VARCHAR(10),
  ref_id_user INT REFERENCES users(id_user),
  date_create timestamp without time zone DEFAULT timezone('utc'::text, now()),
  reprieve_time INT,
  comment text,
  ref_id_order_status INT REFERENCES order_status(id_order_status),
  ref_id_coffee_shop INT REFERENCES coffee_shop(id_coffee_shop)
);

CREATE TABLE order_product(
  id_order_product INT PRIMARY KEY DEFAULT nextval('order_product_id'),
  price DECIMAL,
  title VARCHAR(100),
  other_information json,
  quantity INT,
  ref_id_order INT REFERENCES orders(id_order)
);

CREATE TABLE favorite_product(
  ref_id_user INT REFERENCES users(id_user),
  ref_id_product INT  REFERENCES product(id_product) ON DELETE CASCADE,
  PRIMARY KEY (ref_id_product, ref_id_user)
);

CREATE TABLE favorite_shop(
  ref_id_user INT REFERENCES users(id_user),
  ref_id_shop INT REFERENCES coffee_shop(id_coffee_shop) ON DELETE CASCADE,
  PRIMARY KEY (ref_id_user, ref_id_shop)
);

CREATE TABLE bonus_for_coffee(
  ref_id_product_order INT REFERENCES order_product(id_order_product) ON DELETE CASCADE ,
  ref_id_user INT REFERENCES users(id_user) ON DELETE CASCADE,
  count INT,
  ref_id_network INT REFERENCES coffee_shop_network(id_coffee_shop_network) ON DELETE CASCADE,
  PRIMARY KEY (ref_id_user, ref_id_product_order)
);

CREATE TABLE comments_write_off(
  id_comments_write_off INT PRIMARY KEY DEFAULT nextval('comments_write_off_id'),
  ref_id_coffee_shop INT REFERENCES coffee_shop(id_coffee_shop),
  date timestamp without time zone DEFAULT timezone('utc'::text, now()),
  write_off DECIMAL,
  description text
);


INSERT INTO category_type (id_category_type, type_category) VALUES (DEFAULT , 'напитки');
INSERT INTO category_type (id_category_type, type_category) VALUES (DEFAULT , 'добавки');
INSERT INTO category_type (id_category_type, type_category) VALUES (DEFAULT , 'прочее');

INSERT INTO order_status (id_order_status, status) VALUES (DEFAULT,'новый');
INSERT INTO order_status (id_order_status, status) VALUES (DEFAULT,'принятый');
INSERT INTO order_status (id_order_status, status) VALUES (DEFAULT,'выполненный');
INSERT INTO order_status (id_order_status, status) VALUES (DEFAULT,'отмененный');

*/